package blog.menuTests;


import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.ModelAndView;

import blog.controllers.AjaxElementsController;
import blog.controllers.AjaxPostController;
import blog.controllers.MainController;
import blog.controllers.Utils;
import blog.entities.Post;
import blog.entities.User;
import blog.hibernate.PostDAO;
import blog.hibernate.UserDAO;

/**
 *	Bunch of test with selenium vor various purposes.,
 *check the method names and javadoc for more info
 *
  */

//Let's import Mockito statically so that the code looks clearer
import static org.mockito.Mockito.*;

/**
 * Hard testing when there are sttic methods inside the code i.e. for shortcoding. - TO DO (to check it out)
 * @author TalentLab1
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MockControllerTest {



@Mock	ModelAndView mockMav = new ModelAndView();
@Spy    ModelAndView mockMavSpy ;
@Mock	HttpServletRequest mockReq =null;	
@Mock	PostDAO mockPdao;
@Mock   UserDAO mockUdao;
@Mock	List<Post> mockPl = new ArrayList<>();
@Mock 	SeleniumMenuAdminTest mockUtl2 = new SeleniumMenuAdminTest();

		String mockString = "mocked test string";

 MainController mc = new MainControllerStub();
 AjaxElementsController ac = new AjaxElementsController();
 AjaxElementsControllerStub acs = new AjaxElementsControllerStub();
 AjaxPostController apc = new AjaxPostController();
 
/**
 * 	   public void  AjaxControllerTest.testAjax()
*/

	@Test
   public void  testAjaxTest(){		
		//given
		//when
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		String result = ac.testAjax("");
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		int int1 = ac.getRandomString(6).length();
		int int2 = ac.getRandomString(0).length();
//then	
Assert.assertEquals("assert ", 6, int1);
Assert.assertEquals("assert ", 0, int2);
Assert.assertEquals("assert ", 13, result.length());

	}
	/**	
	 * Stubbed class for mocking purposes.
	 * overriden method in the original class won`t be marked as covered (red)
	 */
	class AjaxElementsControllerStub extends AjaxElementsController {
		@Override
		public String getRandomString(int len) {
			return mockString;
		}
	}
	
	

	/**
	 * Check the login controller
	 */

//	@Test
// 	public void  MainControllerLoginPageTest(){		
//
//		//gettting the list for test purpose
//		//mocking it by using the same code - stupid but works
//		List<Post> pl = new PostDAO().getAllPosts();
//		if (pl  == null)	MockMav.addObject("postEMPTY" , "postListIsNull");
//		else 				MockMav.addObject("PostsList" , pl );
////when the Controller method addPostList(mav) is called
//		//injected mocked ModelAndView with the PostList from above 
////when ( mc.addPostsList(MockMav)	).thenReturn(	 MockMav	);
//		ModelAndView result = mc.addPostsList(MockMav);
////set the name View to the mocked mav
//
////MockMav.setViewName("index");
//
////check the equality of ModelAndView objects
////Assert.assertEquals("assert ",MockMav,mc.indexWithLoginFormn(MockMav, MockReq));
////		verify(mock)
//}

	
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
 	public void checkThatControllerAddsEmptyPostToPostList_WhenThereAreNoPostsInDB(){//_byMg_stubVersion(){		
		//given
		MainController controller = new MainController();
		PostDAO mockDao = mock(PostDAO.class);
		controller.dao = mockDao;
		ModelAndView mav = new ModelAndView();
		
		//when
		when(mockDao.getAllPosts()).thenReturn(new ArrayList());
		ModelAndView result = controller.addPostsList(mav);
		
		//then
		//Istnieje pusty post
		Assert.assertTrue("We need empty post if there are no posts", result.getModel().containsKey("postEMPTY"));
		Assert.assertFalse(result.getModel().containsKey("PostsList"));
		
		//NULL IS TREATED AS EMPTY
		//when
		when(mockDao.getAllPosts()).thenReturn(null);
		result = controller.addPostsList(mav);
		
		//then
		//There is an empty post inside
		Assert.assertTrue("We need empty post if we get null for post list", result.getModel().containsKey("postEMPTY"));
		Assert.assertFalse(result.getModel().containsKey("PostsList"));
	}

	@Test
 	public void checkThatControllerAddsPosts_IfTheyAreInDB(){//_byMg_stubVersion(){		
		//given
		MainController controller = new MainController();//MainController - object to be tested
		PostDAO mockDao = mock(PostDAO.class);			//post mock dao
		controller.dao = mockDao;						//set the DAO of MC to the MockedDAO
		ModelAndView mav = new ModelAndView();			//mav as a reference
		
		
		ArrayList<Post> posts = new ArrayList<Post>();	//create posts list
		posts.add(Post.getNewPost());					//add post to the list
		when(mockDao.getAllPosts()).thenReturn(posts);	//if mockedPostDAO want all posts return the single post
		
		//when
		
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = controller.addPostsList(mav);		//
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		//then
		Assert.assertTrue("If posts exists they should be added to MAV", result.getModel().containsKey("PostsList"));
		Assert.assertEquals("No posts should be added or removed", 1, ((List)result.getModel().get("PostsList")).size());
	}
	
	
	
	/**checkAjaxControllerFor10PostsReturnedInAList	<br />
	 * added PostDAo to the controller parameters,  <br />
	 * we can multiple times run controller method and store the answer <br />
	 * in the same mav object, "when`s" written after asserts are overwriting <br /> 
	 * the previous ones 	
	 */
	@Test
 	public void checkAjaxControllerFor10PostsReturnedInAList(){//_byMg_stubVersion(){		
		//given
		ModelAndView mav = new ModelAndView();	
		mockPdao.setHibfile("testhibernate.cfg.xml");
		List <Post> postslist = new ArrayList<>();
		postslist.add(0, Post.getNewPost());
		
		when( mockPdao.getAllPostsBy10ForPaginationAjaxRequest(0, 10)).thenReturn(postslist);
		when( mockPdao.getPostsNumberInDB(anyInt())).thenReturn(1);
		//when
		//the returned controller after the method is the result , validate in asserts for criteria
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = apc.show10PostsStartingFromFirst(0, mav,mockPdao);
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	System.out.println(postslist.size());
		//then

Assert.assertEquals("View name is different ",
		"posts/showajax" ,result.getViewName()	);
Assert.assertEquals("Check list added in the controller or mock ",
		postslist ,result.getModel().get( "PostsList" )	);
//empty list   ( new list  )
when( mockPdao.getAllPostsBy10ForPaginationAjaxRequest(0, 10)).thenReturn(new ArrayList<Post>());
result = apc.show10PostsStartingFromFirst(0, mav,mockPdao);
Assert.assertEquals("Check list added in the controller or mock ",
		"postEMPTY" ,result.getModel().get( "postEMPTY" )	);

//null


when( mockPdao.getAllPostsBy10ForPaginationAjaxRequest(0, 10)).thenReturn(null);
result = apc.show10PostsStartingFromFirst(0, mav,mockPdao);
Assert.assertEquals("Check list added in the controller or mock ",
		 "postEMPTY" ,result.getModel().get( "postEMPTY" )	);

//4 branches covered :D YAY !




	}
	
	
	
	/**returnAndShowNext10Posts	<br />
	 */
	@Test
 	public void returnAndShowNext10Posts(){//_byMg_stubVersion(){		
		//given
		ModelAndView mav = new ModelAndView();	
		mockPdao.setHibfile("testhibernate.cfg.xml");
		List <Post> postslist = new ArrayList<>();
		postslist.add(0, Post.getNewPost());
		
		when( mockPdao.getAllPostsBy10ForPaginationAjaxRequest(0, 10)).thenReturn(postslist);
		when( mockPdao.getPostsNumberInDB(anyInt())).thenReturn(1);
		//when
		//the returned controller after the method is the result , validate in a sserts for criteria
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = apc.returnAndShowNext10Posts(0, mav,mockPdao);
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		System.out.println(postslist.size());
		//then
	
//view name test 
Assert.assertEquals("View name is different ",
		"posts/showajaxList" ,result.getViewName()	);
Assert.assertEquals("Check list added in the controller or mock ",
		postslist ,result.getModel().get( "PostsList" )	);
//empty list   ( new list  )
when( mockPdao.getAllPostsBy10ForPaginationAjaxRequest(0, 10)).thenReturn(new ArrayList<Post>());
result = apc.returnAndShowNext10Posts(0, mav,mockPdao);
Assert.assertEquals("Check list added in the controller or mock ",
		"postEMPTY" ,result.getModel().get( "postEMPTY" )	);

//null
when( mockPdao.getAllPostsBy10ForPaginationAjaxRequest(0, 10)).thenReturn(null);
result = apc.returnAndShowNext10Posts(0, mav,mockPdao);
Assert.assertEquals("Check list added in the controller or mock ",
		 "postEMPTY" ,result.getModel().get( "postEMPTY" )	);

//4 branches covered :D YAY !


	}



	
	
	
	/**	Test Main Controller (pojo with mocking)	<br />
	 * indexWithLoginForm method
	 */
	@Test
 	public void checkMainController_IndexWithLoginForm_method(){		
		//given
		ModelAndView mav = new ModelAndView();	
		//the returned controller after the method is the result , validate in a sserts for criteria
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = mc.indexWithLoginForm( mav, null );
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//then
		//view name test 
		Assert.assertEquals("View name is different ","index" ,result.getViewName()	);
	}
	
	
	
	
	/**
	 * 
	 * MainControllerindexWithoutLoginTest
	 * 
	 * Check the main controller in " / " the root level of the page.
	 * Full test - all branches covered.
	 * 
	 */

    MainControllerStub mcs = new MainControllerStub();
	@Mock User mockUser ;
	@Mock HttpSession mockHttpSess;
	@Mock Authentication mockAuth=new UsernamePasswordAuthenticationToken(mockUser,null);
	@Mock Utils mockUtl = mcs.getUtl();
	@Test
 	public void MainControllerindexWithoutLoginTest(){
		//given
        ModelAndView mav = new ModelAndView();	
		mockPdao.setHibfile("testhibernate.cfg.xml");
		mockUdao.setHibfile("testhibernate.cfg.xml");
		
		List <Post> postslist = new ArrayList<>();
			postslist.add(0, Post.getNewPost());
		
		List <String> authList = new ArrayList<>();
			authList.add(0, "test name");
			authList.add(1, "test name");
		
		when( mockPdao.getAllPosts()).thenReturn(postslist);
		//System.out.println("Lista gdy mokujemy ="+postslist.get(0).getContent());
		when( mockPdao.getPostsNumberInDB(anyInt())).thenReturn(1);
		when( mockAuth.isAuthenticated()).thenReturn(true);
		when( mockAuth.getName()).thenReturn("testName");
		when( mockUdao.getUserByString("testName"))
			.thenReturn(mockUser);
		when( mockUdao.getUserByString("testName").getId())
			.thenReturn((long) 123);
		when( mockUtl.getLoggedUsername())
			.thenReturn(authList );

		mcs.setIfUserIsLoggingIn(true);
		mcs.setUtl(mockUtl);
		
		//when
		//the returned controller after the method is the result , validate in a sserts for criteria
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = mcs.indexWithoutLogin(
mockUser, null , mav, mockHttpSess, mockUdao,mockPdao	);
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//then
Assert.assertEquals("View name is different ",
		"indexWithLogin" ,result.getViewName()	);
Assert.assertEquals("Check list added in the controller ",
		postslist ,result.getModel().get( "PostsList" )	);

//covering the rest code - if user auth = else

when( mockAuth.isAuthenticated()).thenReturn(false);

result = mcs.indexWithoutLogin(
mockUser, null , mav, mockHttpSess, mockUdao,mockPdao	);

Assert.assertEquals("View name is different ",
"testName" ,result.getModel().get( "username" )	);
Assert.assertEquals("View name is different ",
		"index" ,result.getViewName()	);
	

//if list is a null
when( mockAuth.isAuthenticated()).thenReturn(true);
when( mockPdao.getAllPosts()).thenReturn(null);

result = mcs.indexWithoutLogin(
mockUser, null , mav, mockHttpSess, mockUdao,mockPdao	);

Assert.assertEquals("View name is different ",
"postListIsNull" ,result.getModel().get( "postEMPTY" )	);

	}
	
	/**
	 * Extended clas for testing purposes <br />
	 * overrides method getAuthentication that returns the Authentication object
	 * needed for mocking
	 * @author TalentLab1
	 *
	 */
	class MainControllerStub extends MainController {
		@Override
		protected Authentication getAuthentication() {
			return mockAuth;
		}
	}
	
}








