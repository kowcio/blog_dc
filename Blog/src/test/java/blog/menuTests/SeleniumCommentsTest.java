package blog.menuTests;


import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

 /**
  * Test for links and form from menu concerning posts - adding , deleting etc.
  * @author TalentLab1
  * TO DO : 
  * Make all test as components - run adding, deleting and editing from different methods and 
  * make them integration tests easier without copying the code 
  *
  */
public class SeleniumCommentsTest {
//constants for tests
				String context = "/Blog";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String addurl = "http://localhost:8080"+context;
				String indexurl = "http://localhost:8080"+context;

	/**
	 * 
	 * Add the comment as anonymous user, approve it as admin and check if it 
	 * shows in the main view when nobody is logged in.
	 * 
	 */
	
	@SuppressWarnings({ "static-access" })
	@Test
	public void testPostAddingAndCountIndexPosts() {

		
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);

//commentsSizeBefore 
        List<WebElement> postsCountBefore = driver.findElements(By.id("commentsTable"));
        int commentsSizeBefore =  postsCountBefore.size();
         						
//clicking to add post form and getting the post id        
        WebElement commentgetPostId =  (WebElement) driver.findElement(By.name("addCommentLink"));
        String[] strPostID = commentgetPostId.getAttribute("id").split("Link");
        int postid = Integer.parseInt(strPostID[1]);
        commentgetPostId.click();
        sel.waitForPageToLoad(timePageLoad);
//add post form
        sel.type("nick","12345"+getRandomString(4) ) ;try{ Thread.currentThread().sleep(waitTime);	}
		catch(InterruptedException ie){	}
        sel.type("mail","12345"+getRandomString(4)) ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}  
        sel.type("content","12345"+getRandomString(4)) ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}  
        
//save post        
        sel.click("submit");       
        sel.waitForPageToLoad(timePageLoad);
//go to comments page        
        driver.get(indexurl);
        loginToSystem(sel);
        sel.click("link_showComments");
//click to add post 
        WebElement commentAddLink =  (WebElement) driver.findElement(By.name("publicCommentLink"));
        
        String[] strCommID = commentAddLink.getAttribute("id").split("Link");
        int commid = Integer.parseInt(strCommID[1]);
       
        
        
        sel.click("publicCommentLink"+commid);
        driver.get(indexurl);
        
//check msg for success 
        List<WebElement> comentsCount = driver.findElements(By.className("commentsTable"));
        int commentsAfterAdding =  comentsCount.size();

Assert.assertEquals("Comments number " , commentsSizeBefore+1 , commentsAfterAdding );
		

//  Delete addes test posts - sel.open url to del ?
		WebElement deleteLink = driver.findElement(By.id("deleteLink"+postid));
		deleteLink.click();
        driver.switchTo().alert().accept();
        driver.switchTo().alert().accept();


        driver.close();
        driver.quit();
       
    }
	
	
	
	
	// UTILS and short codes.
	

	/**
	 * Login to system as user with specified role types (authorities)
	 * @param User
	 */
	   public void loginToSystem(WebDriverBackedSelenium sel){
		   System.out.println("loginToSystemStartMethod!! !! ");
	        sel.type("j_username",ulog) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);	}
			catch(InterruptedException ie){	}
	        sel.type("j_password",upass) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);}
			catch(InterruptedException ie){	}
	        sel.click("submit");
	        sel.waitForPageToLoad(timePageLoad);
	        System.out.println("after pageLoad ");
	
	   }
	
		/**
		 * Login to system start from initiating webdriver with index Url,		<br />
		 * after loggin we r at the main page, admin logging default			<br />
		 * @param User
		 */
		   public void loginToSystemMoreCode(String baseUrl){
			   	WebDriver driver = new FirefoxDriver();
		        driver.get(url);
		        WebDriverBackedSelenium sel1= new WebDriverBackedSelenium(driver, url);	
		        System.out.println("loginToSystemStartMethod!! !! ");
		        sel1.type("j_username",ulog) ;try{ Thread.currentThread();
				Thread.sleep(waitTime);	}
				catch(InterruptedException ie){	}
		        sel1.type("j_password",upass) ;try{ Thread.currentThread();
				Thread.sleep(waitTime);}
				catch(InterruptedException ie){	}
		        sel1.click("submit");
		        sel1.waitForPageToLoad(timePageLoad);
		        System.out.println("after pageLoad ");		
		   }
	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	   
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   

	 //setters getters 
	 	   
	 	    

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}
	










}








