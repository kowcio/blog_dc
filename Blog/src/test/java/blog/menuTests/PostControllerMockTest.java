package blog.menuTests;


import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import blog.controllers.PostController;
import blog.controllers.Utils;
import blog.entities.Post;
import blog.entities.User;
import blog.hibernate.PostDAO;
import blog.hibernate.UserDAO;

/**
 *	Bunch of test with selenium vor various purposes.,
 *check the method names and javadoc for more info
 *
  */

//Let's import Mockito statically so that the code looks clearer
import static org.mockito.Mockito.*;

/**
 * Hard testing when there are sttic methods inside the code i.e. for shortcoding. - TO DO (to check it out)
 * @author TalentLab1
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PostControllerMockTest {

@Mock	ModelAndView mockMav = new ModelAndView();
@Mock	HttpServletRequest mockReq = null;	
@Mock	PostDAO mockPdao;
@Mock	Post mockPost;
@Mock   UserDAO mockUdao;
@Mock 	User mockUser;
@Mock	List<Post> mockPl = new ArrayList<>();
@Mock 	SeleniumMenuAdminTest mockUtl2 = new SeleniumMenuAdminTest();
@Mock 	HttpSession mockHttpSess;
@Mock 	Authentication mockAuth = new UsernamePasswordAuthenticationToken(mockUser,null);
@Mock   BindingResult mockBResult;
@Mock   Utils mockUtl;
//controller to test
 PostController pc = new PostController();
 //result objects 
 ModelAndView mav = new ModelAndView();	
	/**
	 * postAddPOSTReturnResultTest				<br />
	 * Check the PostController methods with mocking 	<br />
	 * 
	 */
	@Test
 	public void postAddPOSTReturnResultTest(){
        ModelAndView mav = new ModelAndView();	
		mockPdao.setHibfile("testhibernate.cfg.xml");
		mockUdao.setHibfile("testhibernate.cfg.xml");
		
		
//given				 if (result.hasErrors()) = true
		when( mockBResult.hasErrors()).thenReturn(true);
		when( mockPdao.savePost( mockPost)).thenReturn(123);
		//when
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = pc.postAddPOSTReturnResult(
mockPost, mockBResult, mav, mockReq, mockPdao, mockUdao, mockUser, mockUtl);
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//then
Assert.assertEquals("View name is different ",
		"/posts/add" ,result.getViewName()	);
Assert.assertEquals("View name is different ",
		"input_error" ,mav.getModelMap().get("saveStatus") );

//given			 if (result.hasErrors()) = false - BIG BRANCH
//				req.getParameter("persist")!=null) 	
when( mockBResult.hasErrors()).thenReturn(false);
when( mockReq.getParameter("persist")).thenReturn("not null");
when( mockReq.getParameter("public")).thenReturn("non null");
	List <String> stringList = new ArrayList<>();
	stringList.add(0, "test name");
	stringList.add(1, "test name");
when( mockUtl.getLoggedUsername()).thenReturn(stringList);
when( mockUdao.getUserByString(anyString()) ).thenReturn(new User());
when( mockPdao.savePost(  mockPost )).thenReturn(-123456);

//when
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
result = pc.postAddPOSTReturnResult(
mockPost, mockBResult, mav, mockReq, mockPdao, mockUdao, mockUser, mockUtl);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//then
Assert.assertEquals("View name is different ",
		"/posts/add" ,result.getViewName()	);
Assert.assertEquals("View name is different ",
		"save_notok" ,mav.getModelMap().get("saveStatus") );
Post checkPostFromMAV = (Post) result.getModelMap().get("post"); 
Assert.assertEquals("View name is different ",
		0 ,checkPostFromMAV.getViewType() );
Assert.assertEquals("View name is different ",
		0 ,checkPostFromMAV.getId() );


//req.getParameter("persist")!=null) postid >0		
//	given
when( mockBResult.hasErrors()).thenReturn(false);
when( mockReq.getParameter("persist")).thenReturn("not null");
when( mockReq.getParameter("public")).thenReturn(null);
when( mockPdao.savePost(  mockPost )).thenReturn(123);

//when
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
result = pc.postAddPOSTReturnResult(
mockPost, mockBResult, mav, mockReq, mockPdao, mockUdao, mockUser, mockUtl);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//then
Assert.assertEquals("View name is different ",
"/posts/add" ,result.getViewName()	);
Assert.assertEquals("View name is different ",
		"save_ok" ,mav.getModelMap().get("saveStatus") );

 
// postid < 0
when( mockReq.getParameter("persist")).thenReturn(null);
when( mockPdao.savePost(  mockPost )).thenReturn(123);
result = pc.postAddPOSTReturnResult(
mockPost, mockBResult, mav, mockReq, mockPdao, mockUdao, mockUser, mockUtl
);
Assert.assertEquals("View name is different ",
"/posts/add" ,result.getViewName()	);

	}


	
	
	
	
	/**
	 * PostShowAllTest				<br />
	 * Check the PostController  postShowAll method with mocking 	<br />
	 * 
	 */
	@Test
 	public void postShowAllTest(){
//given	
		when( mockPdao.getPostByID(anyInt())).thenReturn(null);
//when
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ModelAndView result = pc.postShowAll("123", mockPdao, mav, mockPost	);
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//then
Assert.assertEquals("View name is different ",
		"/posts/postNo" ,result.getViewName()	);

//other branch if post == null
//given
mockPost = Post.getNewPost();
when( mockPdao.getPostByID(anyInt())).thenReturn(mockPost);
//when
result = pc.postShowAll("123", mockPdao, mav, mockPost	);
//then
Assert.assertNotEquals("View name is different ",
		null ,result.getModelMap().get("post")	);


	}
	
	
	
	
	
	
}








