package blog.menuTests;


import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionFactoryImpl;
import org.junit.Assert;
import org.junit.Test;
import blog.entities.User;
import blog.hibernate.HibGetDBSession;

 /**
  * Tests for user class
  * 
  * @author TalentLab1
  *
  */

public class UserTest {

	String hibfile = "testhibernate.cfg.xml"; 


	/**
	 * 
	 * Test user object values
	 * 
	 */
		
	@Test
	public void userTestValues() {
	
		User us = new User();
		us.setLogin("login");
		Assert.assertEquals("login", us.getLogin());
}

	@Test
	public void getCustomUserTest() {
	
		User us = new User();
			us.setId((long) 0);
			us.setLogin("admin");
			us.setPasswd("admin");
			us.setEnabled(1);
			us.setUserType(666);
		
		User us1 = new User();
		us1.getCustomUser((long) 0, "admin", "admin", 666, 1);

		Assert.assertEquals(us.getLogin(), us1.getLogin());
		Assert.assertEquals(us.getPasswd(), us1.getPasswd());
//howto ?	== > create custom equals function or do it like aboove
		//Assert.assertEquals(us, us1);
		
		Assert.assertEquals(us.toString(), us1.toString());

}
	
	
	/**Integration test
	 *  Adding user, and using wxternal MenuAdminTest to check if we can log in with him.
	 */
@Test
	public void checkAddAndGetUser(){
    
	SeleniumMenuAdminTest mat = new SeleniumMenuAdminTest();
		User us = new User();
		us.getCustomUser((long) 0, "test_"+mat.getRandomString(3), "test", 666, 1);
		hibfile = "hibernate.cfg.xml";
		
		User dummyUser = new User();
		
		System.out.println("generated user login == "+ us.getLogin());

		SessionFactory sf = new HibGetDBSession().getAnnotationsSessionFactory();
		//save a post    	
		
		
	try {
			Session session = sf.openSession();
			Transaction tx = session.beginTransaction();
				session.save(us);
			tx.commit();
	} catch (Exception e) {
				System.out.println("Error saving single user -UserTest.java  ");
				e.printStackTrace();
		}
		
	
		
	try {	
			Session session1 = sf.openSession();
			Transaction tx1 = session1.beginTransaction();
				dummyUser = (User) session1.get(User.class, us.getId());
			tx1.commit();
	} catch (Exception e) {
				System.out.println("Error saving single user -UserTest.java  ");
				e.printStackTrace();
	}


	Session ses = sf.openSession();

	
	SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) sf;
	Properties props = sessionFactoryImpl.getProperties();
	String url = props.get("hibernate.connection.url").toString();
	String[] urlArray = url.split(":");
	String db_name = urlArray[urlArray.length - 1];	
	
	System.out.println("db_name = "+db_name);
	
	
		
		
			Assert.assertEquals(	us.toString(), dummyUser.toString()	) ;
		
 	

		
	
        
	}
	
	
	
	}




