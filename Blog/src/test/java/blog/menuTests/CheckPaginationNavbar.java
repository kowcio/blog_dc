package blog.menuTests;


import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *	Bunch of test with selenium vor various purposes.,
 *check the method names and javadoc for more info
 *
  */


public class CheckPaginationNavbar {

//constants for tests
				String context = "/Blog/";
				//String context = "/";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String notepadurl = "http://localhost:8080"+context+"notepad";
				String indexurl = "http://localhost:8080"+context;
				String logouturl = "http://127.0.0.1:8080"+context+"j_spring_security_logout";
//utils from the other test class				
				SeleniumMenuAdminTest utl = new SeleniumMenuAdminTest();	
				
				
/**
 * First attempt of TDD
 */
	@Test
   public void  checkThePagination10PostsPerPage   (){
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);        	

        @SuppressWarnings("unchecked")
        String name="PostTable";
		List <WebElement> posts = (List<WebElement>) driver.findElements(By.className(name)  );
        Assert.assertTrue( 10  <=  posts.size() );

        List <WebElement> navBars = (List<WebElement>) driver.findElements(By.className("pageBarNo")  );
        navBars.get(2).click();
        
        List <WebElement> postsOnSecondPage = (List<WebElement>) driver.findElements(By.className(name)  );
        Assert.assertTrue( 10  <=  postsOnSecondPage.size() );

       
        
        driver.close();
        driver.quit();
        
    }
	 	   
	
	@Test
	public void checkTheChanginNavbarForPagination(){
		   WebDriver driver = new FirefoxDriver();
	        driver.get(indexurl);
	        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);        	
	        String name="pageBarNo";
	      
			List <WebElement> navBars = (List<WebElement>) driver.findElements(By.className(name)  );
//original navbar		    
		    for ( int i=1 ; i<navBars.size() ; i++ ) {
		    	Assert.assertEquals("Checking the number on navBarNo button", i , Integer.parseInt(navBars.get(i-1).getText()) );
		    }
		    
//click on the 4th page and check the values in navbar
		    int clickThePage = 4;
		    navBars.get(clickThePage).click();
			List <WebElement> navBarsFrom4 = (List<WebElement>) driver.findElements(By.className(name)  );
//original navbar		    
		    for ( int i=0 , j=clickThePage-1 ; i<navBarsFrom4.size() ; i++,j++ ) {
		    	Assert.assertEquals("Checking the number on navBarNo4 ", j , Integer.parseInt(navBarsFrom4.get(i).getText()) );
		    }	
		    
//click on the 4th page and check the values in navbar
		    int clickThePageAgain = 3;
		    navBarsFrom4.get(clickThePageAgain).click();
			List <WebElement> navBarsFrom3 = (List<WebElement>) driver.findElements(By.className(name)  );
//original navbar		    
		    for ( int i=0 , j=clickThePage ; i<navBarsFrom4.size() ; i++,j++ ) {
		    	Assert.assertEquals("Checking the number on navBarNo3 ", j , Integer.parseInt(navBarsFrom3.get(i).getText()) );
		    }
		    driver.close();
	        driver.quit();
	}
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	//setters and getters

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}




	public String getContext() {
		return context;
	}




	public void setContext(String context) {
		this.context = context;
	}





	public String getIndexurl() {
		return indexurl;
	}




	public void setIndexurl(String indexurl) {
		this.indexurl = indexurl;
	}




	public String getLogouturl() {
		return logouturl;
	}




	public void setLogouturl(String logouturl) {
		this.logouturl = logouturl;
	}


	










}








