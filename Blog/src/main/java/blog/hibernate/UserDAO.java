package blog.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import blog.entities.User;

public class UserDAO {

	HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	String hibfile = "hibernate.cfg.xml"; 

	
	/**
	 *Save a User  class  into the user  table
	 *
	 * @param  User 
	 * @return 
	 */
	
	public String saveUser(User user){
	   	try {
	   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
	   		Transaction tx = session.beginTransaction();
			user.toString();
			session.save(user);
			tx.commit();
			return "OK";
		} catch (Exception e) {
			System.out.println("Error saving user "+e.getCause());
			 e.printStackTrace();
			 return "notOK";
		}
	}   	
	
	
	/**
	 *Delete a User  class from  the user  table
	 *
	 * @param  User  
	 * @return OK / notOK String
	 */
	
	public String deleteUser(User user){
	   	try {
	   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
	   		Transaction tx = session.beginTransaction();
			user.toString();
			session.delete(user);
			tx.commit();
			return "OK";
		} catch (Exception e) {
			System.out.println("Error deleting user "+e.getCause());
			 e.printStackTrace();
			 return "notOK";
		}
	}
	
	/**
	 * 
	 * @return All Users as list.
	 */
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers(){
 
		List<User> UsersList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			UsersList = (List<User>)session.createQuery("from User").list();
		   	System.out.println("UsersList count = "+UsersList.size());
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - UserDAO - getAllUsers");
			 e.printStackTrace();
		}
	   	return UsersList;
	}

	/**
	 * Return User by ID.
	 * 
	 * @param UserId - Users ID which we wish to load.
	 * @return	Specified User
	 */
	
	
	public User getUserByID(Integer UserId){
 
		User User =new User();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			User = (User) session.get(User.class, UserId);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - getUserByID");
			 e.printStackTrace();
		}
	 	System.out.println("Pobrano User, id = "+UserId);
	   	return User;
	}

/**	Update in db a User object.
 * 
 * @param User User which we wish to update. Needs to have a specified ID.
 */

	public void updateUser(User User){
 
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(User);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - > - getUserByID");
			 e.printStackTrace();
		}
		System.out.println("Update User = "+User.toString());
	}



	
	/**	Get user object by name
	 * 
	 * @return User by his name...
	 */
	@SuppressWarnings("rawtypes")
	public User getUserByString(String login){
 
		User user =new User();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			System.out.println(" Login to get = " + login);
			String sql = "FROM User WHERE login = '"+login+"'";
			Query query = session.createQuery(sql);
			List results = query.list();
			user = (User) results.get(0);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Error - UserDAO ->  getUserByID");
			 e.printStackTrace();
		}
	   	return user;
	}


	
	
	
	

	//setters getters
	

	public HibGetDBSession getFabrykaHibernejta() {
		return fabrykaHibernejta;
	}



	public void setFabrykaHibernejta(HibGetDBSession fabrykaHibernejta) {
		this.fabrykaHibernejta = fabrykaHibernejta;
	}


	public String getHibfile() {
		return hibfile;
	}


	public void setHibfile(String hibfile) {
		this.hibfile = hibfile;
	}
	

	
	
	
	
	
}
