package blog.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

/**
 * TO DO : IMPLEMENT
 * Basically to populate db with user types / roles for spring security.
 * @author TalentLab1
 *
 */
@Deprecated
public class UserTypes {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
    private Long id;
	
	@Length(min=3, max=10, message="Login requires 3-10 characters")
	@Pattern(regexp="^[a-z0-9_-]{3,15}$",message="Username must match : a-z, 0-9, _, -, 3-15 chars")
	@Column(unique=true)//check if this or manually handle checking uniqueness in the db
	private String login;
	
	@Length(min=3, max=10, message="Password requires 3-10 characters")
    private String passwd;
    
	
	private int userType = 0; //default user is a normal user - userType=0


	
	// setters getter
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPasswd() {
		return passwd;
	}


	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}


	public int getUserType() {
		return userType;
	}


	public void setUserType(int userType) {
		this.userType = userType;
	}
	
	

	
	
	
}
