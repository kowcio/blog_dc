package blog.controllers;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import blog.entities.Post;
import blog.hibernate.PostDAO;

/**
 * Controller for /posts/ urls
 * 
 * @author TalentLab1
 *
 */

@Controller
public class NotepadController {
	
	
	Logger logger = Logger.getLogger("Iter4Logger");
	Utils utl = new Utils();
   
	
	/**
	 * Show the posts in the notepad 
	 * remaked /posts url to cope with dao method where viewType = 0 ;
	 * @param principal
	 * @return mav
	 * Crashen when we go to it after logging in, need to make it good
	 */
	
	@RequestMapping(value="/notepad",method= RequestMethod.GET)
	public ModelAndView showUsersNotepad(
			HttpSession htses,
			PostDAO dao
		) {
		System.out.println(" Loading /notepad");
		ModelAndView mav = new ModelAndView("posts/notepad");

		if (	htses.getAttribute("userid") == null	) 
			{return new ModelAndView("redirect:/");
			}
		System.out.println("user id from session = "+Integer.parseInt(htses.getAttribute("userid").toString()));
		List <Post> lp = dao.getAllNotepadPosts(0 , 	Integer.parseInt(htses.getAttribute("userid").toString())	);
		if (	lp	 == null) {
			 mav.addObject("postEMPTY" , "postListIsNull");}
		else{ mav.addObject("PostsList" , lp );}

		return mav;
	}
	
	
	/**
	 * Public the post in the notepad for all users
	 * @param principal
	 * @return mav
	 */
	@RequestMapping(value="/notepad/makepublic/{postNo}",method= RequestMethod.GET)
	public String makePublicUserPostWithGivenID(
			@PathVariable("postNo") String postNo,
			HttpSession htses,
			ModelAndView mav
		) {
//		System.out.println(" /notepad/makepublic/"+postNo +  " USER = "+htses.getAttribute("userid")  ) ;
		Integer postNoInt = Integer.valueOf(postNo);
		PostDAO pdao = new PostDAO();
		Post post = pdao.getPostByID(postNoInt);
		post.setViewType(1);
	
		//check if user from session and userid from Post are the same to approve the execution of post update
		int sesUserID = Integer.parseInt(htses.getAttribute("userid").toString());
		if (post.getUserID() == sesUserID){
			pdao.updatePost(	post	);	
		}
		
	
		return "redirect:/notepad";
	}
	
	
	
	
	
	/**
	 * Shows the given post to edit 
	 * @param postNo
	 * @return
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value="/notepad/edit/{postNo}",method= RequestMethod.GET)
	public ModelAndView showUserNotepadPostEditFormToUpdatePost(
			@PathVariable("postNo") int postNo,
			PostDAO pdao,
			Post post,
			ModelAndView mav,
			HttpSession htses
			) {
		//check if user from session and userid from Post are the same to approve the execution of post update
		int sesUserID = Integer.parseInt(htses.getAttribute("userid").toString());
		post = pdao.getPostByID(postNo);
		System.out.println(" user postid  ="+post.getUserID());
		System.out.println(" ses user id  ="+sesUserID);
		if (post.getUserID() == sesUserID){
			System.out.println(" USER logged loading editNotepadPostNo");
			mav.setViewName("/posts/editNotepadPostNo");
			if (post == null)
			{	System.out.println(" USER logged post no");
				mav.addObject("postEMPTY" , "pos with given id = "+postNo +" does not exist in databse");
				mav.setViewName("redirect:/");
				return mav;
			}
			else {
					System.out.println(" USER logged in and post ok");
					mav.addObject("post" , post );
					return mav;	
			}
		}
		else{//not authorize to edit the post
			System.out.println(" USER id != post id ");
			mav.setViewName("redirect:/");
			return mav;
			
		}
		
		

		
	}
	
	/**
	 * Posts edit form for admin user, the old value can be see after editing on the same view.
	 * @param postNo
	 * @return
	 */
	// the GETBug
	@RequestMapping(value="/notepad/edit/{postNO}",method= RequestMethod.POST)
	public ModelAndView saveorUpdateTheEditedUserNotepadPost(
			@ModelAttribute("post") @Valid Post updatedPost, 
			BindingResult result,
			HttpSession htses,
			ModelAndView mav,
			HttpServletRequest req,
			PostDAO dao,	
			Utils utl
			) {
		System.out.println(" Loading form after edit post");
		
		//int sesUserID = Integer.parseInt(htses.getAttribute("userid").toString());
		int vT =  req.getParameter("public")== null ? 0 : 1 ; 
		mav.setViewName("redirect:/notepad");
		mav.addObject("oldpost" , dao.getPostByID(updatedPost.getId()));

		  if (result.hasErrors()){
			  mav.addObject("allNotOK" , "post id = "+updatedPost.getId() +" update had an error");
		  }
		  else
		  {		try {
			  	if (updatedPost.getViewType()==1)
					{updatedPost.setViewType(1);
					}
					else 
					{updatedPost.setViewType(vT);
					}
						dao.updatePost(updatedPost);
						logger.info("User "+utl.getLoggedUsername().get(0)+"edited post ID = "+updatedPost.getId() );
						mav.addObject("post" , updatedPost  );
						mav.addObject("allOK" , "post id = "+updatedPost.getId() +" update OK !!");
						
					} catch (Exception e) {
						mav.addObject("allNotOK" , "post id = "+updatedPost.getId() +" update had an error");
						e.printStackTrace();
					}
		  }
		// this works cool  (but the mock tests won`t do no good ) 
		//return new ModelAndView("redirect:/notepad");
		//but why is this returning the vlaues as get in the url ?!
	  return mav;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//utilities and etc 
	
	/**
	 * Method for casting Long to INT
	 * @param l long
	 * @return int l
	 */
	public static int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	
	
	
	
}