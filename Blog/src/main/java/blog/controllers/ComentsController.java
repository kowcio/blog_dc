package blog.controllers;


import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import blog.entities.Comment;
import blog.hibernate.CommentDAO;
import blog.hibernate.PostDAO;
import blog.mail.MailSendCommentApproval;

/**
 * Controller for /posts/ urls
 * @author TalentLab1
 */

@Controller
public class ComentsController {
    @Autowired  
	 public MessageSource msgSrc;  
		  public void setMessageSource(MessageSource msgSrc) {  
		     this.msgSrc = msgSrc;  
		  } 
		  
	Utils utl = new Utils();
    
	/**
	 * View showing the comments
	 * @return
	 */
	@RequestMapping(value="/comments",method= RequestMethod.GET )
	public ModelAndView showCommentsForAdminToApprove(
			ModelAndView mav,
			CommentDAO cdao
			) {
		//LOGIC 
		mav.setViewName("posts/comments");		
		List<Comment> cl = cdao.getAllNotepadComments(0);
		//variables added to MAV
		if (cl == null || cl.isEmpty()) {
			System.out.println("comments list null or empty");
			mav.addObject("commentsEMPTY","commentsEMPTY");	
		}
		else {System.out.println("not null");
			mav.addObject("commentsList",cl);
		}
	
		return mav;
}
	
	
	
	/**
	 * View showing the comments
	 * @return
	 */
	@RequestMapping(value="/comment/public/{commNo}",method= RequestMethod.GET )
	public ModelAndView approveTheCommentWithID(
			@PathVariable("commNo") String commNo,
			ModelAndView mav,
			CommentDAO cdao,
			Comment comm,
			MailSendCommentApproval msca,
			HttpServletRequest request
			) {
		int comid = Integer.parseInt(commNo);
		comm = cdao.getCommentByID(comid);
		//System.out.println("comment viewType = "+comm.getViewType());
		comm.setViewType(1);
		cdao.updateComment(comm);
		//System.out.println("comment viewType = "+comm.getViewType());
		
		//send mail
		msca.sendCommentOKMail(msgSrc,comm);
		
		mav.setViewName("redirect:/comments");

		return mav;
}
	
	/**
	 *Form to add a comment
	 * @return
	 */
	@RequestMapping(value="/comment/add/{postNo}",method= RequestMethod.GET )
	public ModelAndView loadformToAddCommentToThePost(
			@ModelAttribute("comment") @Valid Comment newComment,
			@PathVariable int postNo,
			ModelAndView mav,
			PostDAO pdao
			) {
//set the comment
		newComment.setPostID(postNo);
		mav.addObject("post", pdao.getPostByID(postNo));
		mav.setViewName("posts/addComment");
//set the post		
		return mav;
}
	
	/**
	 *Logic validating and savingthe post.
	 * @return
	 */
	@RequestMapping(value="/comment/add/",method= RequestMethod.POST )
	public ModelAndView addCommentToThePostAndDatabasePOSTMethod(
			@ModelAttribute("comment") @Valid Comment newComment,
			BindingResult result,
			ModelAndView mav,
			CommentDAO cdao,
			HttpServletRequest request
			) {
//save the comment
		//the postID is stored in the comment object when the get method is 
		//used and the comment model attr passed here in the POST form
		//System.out.println("adding comment"  +newComment.getPostID());
		
		 if (result.hasErrors())
		 {	//get errors
		    mav.setViewName("posts/addComment");
		    // nicely redirect back a page
		    //mav.setViewName("redirect:" + request.getHeader("Referer"));
		  	return mav;
		 }
		 else
		 {
			 
			 LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);                
			 Locale locale = localeResolver.resolveLocale(request);
			 newComment.setLang(locale.toLanguageTag());
			 
		 	cdao.saveComment(newComment)	;
		 	mav.setViewName("redirect:/");
		 }
		
		 	return mav;

}
	
	
	
	
	
	
	
	//utilities and etc 
	
	/**
	 * Method for casting Long to INT
	 * @param l long
	 * @return int l
	 */
	public int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l +
	          " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	
	
	
	
}