<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">


<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>

<head>
   <title>Show Posts Blog Index Page</title>	

 	  <!--  /HiSpring3/resources/css/maincss.css - whole path to css files 	-->
 	   <link href=" <c:url value="/resources/css/maincss.css" />" rel="stylesheet" type="text/css" />
 	   
</head>
<body>




<div id="header" class="header">
	<jsp:include page="/WEB-INF/jsp/header/header.jsp" />
</div>	



<div id="main">



<div id="menu" class="menu">	
			
			<sec:authorize access="isAnonymous()">
				 <jsp:include page="/WEB-INF/jsp/mainsite/loginform.jsp"/>
			</sec:authorize>	
			
			<sec:authorize access="isAuthenticated()">
				<jsp:include page="/WEB-INF/jsp/mainsite/menu.jsp" />
			</sec:authorize>

</div>	



<div id="mainPagePosts">


<div id="showPosts" class="showPosts">	


<c:if test="${commentsEMPTY=='commentsEMPTY'}">
		No comments to display.	
</c:if>
	
<div class="displayPosts">
	<c:forEach var="post" items="${commentsList}">
	
	<div class="singlePostDiv">
				<table class="PostTable">
			 	<tr><td>ID: 	</td><td>        ${post.id} 	</td></tr>
			 	<tr><td>postID:	</td><td>        ${post.postID} 	</td></tr>
			 	<tr><td>Content:</td><td>   	 ${post.content}</td></tr>
		 		<tr><td>  Nick :</td><td>		 ${post.nick}	</td></tr>  	 
		 	 	<sec:authorize access="hasRole('666')">
		 			<tr><td>  Mail :</td><td> 		 ${post.mail}	</td></tr>	
		 		</sec:authorize>

		 	 	</table>
		 	 	<!--  security links displayed for admin or user etc. -->		 	 	
		 	 		
<div class="publicCommentLinkDiv text-right"><h5>	 			
		 	 		<sec:authorize access="hasRole('666')">
								<a href="${path}/comment/public/${post.id}/"
								name="publicCommentLink"
		 	 					id="publicCommentLink${post.id}">
		 	 					<spring:message code="post.add.publicPostCheckBox" text="Public" />   
		 	 					</a>	
					</sec:authorize>	
</h5></div>		
</div>
	</c:forEach>
	
	

</div> <!--  the posts div -->	
</div>
</div>

<!--  end main  -->

</div>

	<div id="footer">
	footer
</div>
	


	
</body>
</html>