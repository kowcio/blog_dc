<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">


<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>



<head>
   <title><spring:message code="index.title" text="ERROR no code" />	</title>	
    	   <!--  favion for jetty error -->
 	   <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/img/favicon.ico"/>" />
	  <!--  /HiSpring3/resources/css/maincss.css - whole path to css files 	-->
 	   <link href=" <c:url value="/resources/css/maincss.css" />" rel="stylesheet" type="text/css" />
 	   
 	   <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.0.0.js"/>" ></script>
 	   <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>" ></script>
 	   <script type="text/javascript" src="<c:url value="/resources/js/jquery.bootpag.js"/>" ></script>
 	   <script type="text/javascript" src="<c:url value="/resources/js/skrypty.js"/>" ></script>

</head>
<body>


<div id="header" class="header">
	<jsp:include page="/WEB-INF/jsp/header/header.jsp" />
</div>	


<div id="main">



<div id="menu" class="menu">	
			
			<sec:authorize access="isAnonymous()">
				 <jsp:include page="/WEB-INF/jsp/mainsite/loginform.jsp"/>
			</sec:authorize>	
			
			<sec:authorize access="isAuthenticated()">
				<jsp:include page="/WEB-INF/jsp/mainsite/menu.jsp" />
			</sec:authorize>

</div>	

    

<div id="mainPagePosts">


 <!--
-->
<c:import url="/posts/part/0"/>

</div>
	
	
</div>
	
	
	<div id="footer">
	footer
</div>
	
	
	
	
</body>
</html>







