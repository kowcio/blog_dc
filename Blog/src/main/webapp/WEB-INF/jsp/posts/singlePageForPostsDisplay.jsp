<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    

<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


	
<div class="displayPosts ">

	<c:forEach var="post" items="${PostsList}">
	
	<div class="singlePostDiv">
				<table class="PostTable span11 table-condensed"><tr>
			 	<td class="pull-center">  ${post.id} 		</td>
			 	<td> 				${post.title}	</td>
			 	<td class="pull-right">	
			 	<fmt:formatDate value="${post.addDate}" pattern="dd/MM/yyyy" />
			 	</td></tr>
			 		<!--  show link when we have more than 300 characters -->
			 		<c:choose>
	   					<c:when test="${post.content.length() > 300}">
	   						<c:set var='postShort' value='${fn:substring(post.content, 0, 300)} ...'/>
	   						<tr><td colspan="3">
	   						<div class="postContent">	
	   						  		${postShort}
							<br />		
	   						<a href="${path}/posts/show/${post.id}/" class="showLink" id="showLink${post.id}">
	   						<spring:message code="post.showWhole" text="Whole Post" />
	   						</a>
	   						</div> 
	   						</td></tr>
	   					</c:when>
	   				<c:otherwise>
			 				<tr><td colspan="3">  		
			 					<div class="postContent">			
			 								${post.content}
			 					</div>					
			 				</td></tr>
	   				</c:otherwise>
					</c:choose>
					
<tr><td colspan="3">
					
<!--  security links displayed for admin or user etc. -->		 	 	
<sec:authorize var="loggedInAdmin" access="hasRole('666')" />
<c:choose><c:when test="${loggedInAdmin}">		
<div class="postsLinks"><h5>	 			
		 	 					<a href="${path}/adminPanel/post/${post.id}/" name="editLink" id="editLink${post.id}">  
		 	 					<spring:message code="posts.edit" text="ERROR no code" />  </a>
		 	 					<a href="${path}/posts/delete/${post.id}/" onclick="return checkMe()" name="deleteLink"
		 	 					id="deleteLink${post.id}">
		 	 					<spring:message code="posts.delete" text="ERROR no code" />   </a>
								<a href="${path}/comment/add/${post.id}/" name="addCommentLink" id="addCommentLink${post.id}">
			 					<spring:message code="post.addcomments" text="Comment" />   </a>
 </h5></div>
 </c:when><c:otherwise>	
<div class="postsLinks"><h5>	 			
								<a href="${path}/comment/add/${post.id}/" name="addCommentLink" id="addCommentLink${post.id}">
 								<spring:message code="post.addcomments" text="Comment" />   </a>
</h5></div>
</c:otherwise></c:choose>
</td></tr>


<tr><td colspan="3">
			<div class="commentsDiv">
			
<c:choose><c:when test="${post.commentsList==null || empty post.commentsList}">		
<div class="well">
	<spring:message code="post.addcomments" text="Comment" />   
</div>

</c:when>
<c:otherwise>

			<c:forEach var="comment" items="${post.commentsList}">
					<div class="well commentsWellWidth">
					<div class="commentID">
					 		${comment.id} 	
					</div><div class="commentNick">
					 		<i class="icon-user" ></i> ${comment.nick}
					 		<br />
							<sec:authorize access="hasRole('666')">
							<i class="icon-envelope" ></i> <a href="mailto:${comment.mail}">${comment.mail}</a>
					 		</sec:authorize>
					</div>
					<div class="commentContent">
					 	  	 ${comment.content} 	
					</div>

					</div>
			</c:forEach>

</c:otherwise>
</c:choose>

	    		
	    	</div>

</td></tr></table>
	    	</div>
	</c:forEach>
	
</div> <!--  the posts div -->	
