<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html;charset=UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>



<head>
   <title>Blog Index Page</title>	

 	  <!--  /HiSpring3/resources/css/maincss.css - whole path to css files 	-->
 	   <link href=" <c:url value="/resources/css/maincss.css" />" rel="stylesheet" type="text/css" />
 	   
</head>
<body>

<div id="header" class="header">	

	<jsp:include page="/WEB-INF/jsp/header/header.jsp" />
	
</div>	





<div id="main">



<div id="menu" class="menu">	
	<jsp:include page="/WEB-INF/jsp/mainsite/menu.jsp" />
</div>	



<div class="mainContent">	


<form:form method="POST" action="${path}/posts/add" modelAttribute="post" id="addPostForm" class="border">
		<table><tr><td>
				
				<form:label path="title">
					<spring:message code="post.add.title" text="Title" />
					</form:label><br />
					<form:input path="title" />
					<br />					
					<form:errors path="title" 	cssClass="error"/>
				</td>
			</tr>
			<tr>		
				<td>
				<form:label path="content"><spring:message code="post.add.content" text="Content" /></form:label><br />
					<form:textarea path="content" rows="5" cols="30"/>
					<br />					
					<form:errors path="content" 	cssClass="error"/>
				</td>
			</tr>
			<tr>
			<td >
				<input type="submit" value="<spring:message code="post.add.addbtn" text="Add" />" id="submit" class="btn"/>
				
				<input type="checkbox" name="persist" value="ok" id="persistCheckBox"/>
				<spring:message code="post.add.savebtn" text="Persist" />
				<input type="checkbox" name="public" value="1" id="publicPostCheckBox"/>
				<spring:message code="post.add.publicPostCheckBox" text="Public" />										
				
			</td>	
			</tr>
			</table>

</form:form>






<div id="savedPost">
Post status :
<c:if test="${saveStatus=='save_ok'}">
		<h3 class="allOK"><spring:message code="post.add.${saveStatus}" text="OK" /></h3>	
</c:if>
<c:if test="${saveStatus=='save_notok' || saveStatus=='input_error'}">
		<h3 class="error"><spring:message code="post.add.${saveStatus}" text="ERROR" /></h3>	
</c:if>	
	

<br />
<br />

<table><tr><td>			 	
ID	 </td><td id="postid">	 ${post.id} 			
</td></tr><tr><td><spring:message code="post.add.title" text="Title" /> 	</td><td>	${post.title}				<br />
</td></tr><tr><td><spring:message code="post.add.content" text="Content" />	</td><td>	${post.content}				<br />
</td></tr><tr><td><spring:message code="post.add.adddate" text="AddDate" />	 </td><td>	${post.addDate}				<br />
</td></tr></table>


</div>



</div>	


</div>

	
	
	<div id="footer">
	footer
</div>





	
</body>
</html>