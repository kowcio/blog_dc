<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page contentType="text/html;charset=UTF-8"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>				</title>
	   	  <link href="<c:url value="/resources/css/maincss.css"/>" rel="stylesheet" type="text/css" />

</head>
<body>




<div id="header" class="header">	

	<jsp:include page="/WEB-INF/jsp/header/header.jsp" />
	
</div>	


<div id="main">


<div id="menu" class="menu">	
	<jsp:include page="/WEB-INF/jsp/mainsite/menu.jsp" />
</div>	



<div class="mainContent">

<!--  <c:url value="/register/reguser"/>  -->

<h2><spring:message code="user.reg.title" text="main page" /></h2>

	<form:form method="post" action="${path}/register/reguser" modelAttribute="user">
	
		<table>	<tr>		
			<td>
			<form:label path="login">Login : </form:label>
			<form:input type="text" path="login"  name="login" value="${login}"/>
			</td>		
			<td> 		
			<form:label path="passwd">Passwd : </form:label>
			<form:input type="text" path="passwd" name="passwd" value="${passwd}"/>						
			</td>
			</tr>
			<tr>	
			<td colspan="2" align="center">
						<input type="submit" id="adduser" value="Add user" name="submit"/>
					</td>	</tr>
					</table>
					<div class="error"> ${allNotOK} </div>
					<form:errors path="login" 	cssClass="error"/><br />
					<form:errors path="passwd"	cssClass="error"/><br />		
</form:form>

	<div id="registerResult">
	
	<spring:message code="user.id" text="ID :" /> <br />
	<spring:message code="user.login" text="Login" /> ${user.login}  <br />
	<spring:message code="user.passwd" text="Password" /> ${user.passwd}<br /> 
	<br />
	
	<div class="allOK"> ${allOK} </div>
	</div>

</div>
</div>

	
		
	<div id="footer">
	footer
</div>
	
	
</body>
</html>
