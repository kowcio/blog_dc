<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>

		
<!--   <a id="register" href="${path}/register/reguser"> Register  </a>		<br />	 -->

<form action="<c:url value='j_spring_security_check' />" class="text-center" method='post'>
		<table>
			<tr>
				<td>	<spring:message code="login.form.user" text="ERROR no code" />
				</td>
				<td><input type='text' name='j_username' value='' class="loginInputForm"/>
				</td>
			</tr>
			<tr>
				<td><spring:message code="login.form.pass" text="ERROR no code" /></td>
				<td><input type='password' name='j_password'  class="loginInputForm"/>
				</td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit" class="btn"  
				value="<spring:message code="login.btn.msg" text="Title" />" />
				</td>
			</tr>
		</table>

	</form>

<div id="logError" class="error"> <br /> ${wrongLogPas} </div>
