<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


<!--  MAIN SITE  -->
<a id="glowna" href="${path}" class="btn"><i class="icon-home" ></i>
<spring:message code="menu.mainpage.link" text="ERROR no menu.mainpage.link code" /></a>
		  
<!--  LOGOUT  -->
<a href="<c:url value="/j_spring_security_logout" />" class="logoutLink btn"><i class="icon-remove" ></i> 
	<spring:message code="menu.logout" text="ERROR no code" />  </a>	<br /><br />




<sec:authorize access="hasRole('666')">

	<a id="alink_console" href="${path}/console/"> 			
	<spring:message code="menu.consoleH2" text="ERROR no code" />	
	</a><br />
	
	<a id="alink_admpanel" href="${path}/adminPanel"> 	
		<spring:message code="menu.adminPanel" text="ERROR no code" />
	</a><br />
	
	<a id="alink_reguser" href="${path}/register/reguser">
	<spring:message code="menu.user.register" text="ERROR no code" />	
	</a><br />
	
	<a id="alink_showlogs" href="${path}/logs">
	<spring:message code="menu.user.logs" text="Log" />	
	</a><br />

		
	<a type="button" id="alink_testajax" href="#">
	<spring:message code="menu.user.testajax" text="testajax" />
	</a>	
	<br />
	
	<a id="link_showComments" href="${path}/comments"> 	
		<spring:message code="menu.show.comments" text="ERROR no code" />
	</a>							<br />
	
</sec:authorize>


	
	<br />
<sec:authorize access="hasAnyRole('0','666')">
<spring:message code="menu.user.menuname" text="ERROR no code" />
<br />
	<a id="link2_addpost" href="${path}/posts/add"> 
		<spring:message code="menu.add.post" text="ERROR no code" />	
 	
	</a>							<br />	
	<!--  NOTEPAD  -->	
	<a id="alink_notepad" href="${path}/notepad">
	<spring:message code="menu.user.notepad" text="Notepad" />	
	</a><br />
	<a id="link2_showposts" href="${path}/posts"> 	
		<spring:message code="menu.show.post" text="ERROR no code" />
	</a>							<br />

</sec:authorize>


	
	
	
	
	
	





