
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html;charset=UTF-8"%>



<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


<!--  LANGUAGES  -->
<div id="langSelector">
<a href="?language=pl" id="lang_pl"><img src="${path}/resources/img/flag_pl_small.png" width="25" height="25" alt="PL"/></a>
<a href="?language=eng" id="lang_eng"><img src="${path}/resources/img/flag_uk_small.png" width="25" height="25" alt="UK"/></a>

<spring:message code="header.current" text="Current" />

<c:set var="localeCode" value="${pageContext.response.locale}" />
 <c:choose>	
      <c:when test="${localeCode=='pl'}">
			<img src="${path}/resources/img/flag_pl_small.png" width="25" height="25" alt="PL"/>
      </c:when>
      <c:otherwise>
			<img src="${path}/resources/img/flag_uk_small.png" width="25" height="25" alt="UK"/>
      </c:otherwise>
</c:choose>


</div>
<div id="greeting">
	welcome.springmvc: <spring:message code="welcome.springmvc" text="ERROR no code" />	   	
</div>


<div id="greetingUser">
<div id="welcomeMsg"><spring:message code="menu.welcome" text="Welcome"/></div> 
		    <sec:authorize access="isAuthenticated()">  
		        <strong>	<sec:authentication property="principal.username"/>	</strong>
		    </sec:authorize>
	   		<sec:authorize access="!isAuthenticated()">  
		        <strong> Unnamed </strong>
		    </sec:authorize>
		
</div>	


