package blog.menuTests;


import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;


/**
 *	Bunch of test with selenium vor various purposes.,
 *check the method names and javadoc for more info
 *
  */

//Let's import Mockito statically so that the code looks clearer
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class MockRequestTUTORIALTest {

//constants for tests
	
				String context = "/Blog/";
				//String context = "/";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String adduserurl = "http://localhost:8080"+context+"register/reguser";
				String indexurl = "http://localhost:8080"+context;
				String logouturl = "http://127.0.0.1:8080"+context+"j_spring_security_logout";
				
		
				
	/**
	 * 
	 * 	Mockito test
	 */

	//mock creation with annotations - less code have to be run with 	
				//=> at class @RunWith(MockitoJUnitRunner.class) or MockitoAnnotations.initMocks(List.class); http://docs.mockito.googlecode.com/hg/latest/org/mockito/Mockito.html#mock_annotation
				
				@Mock  
				private List mockedList ;
				@SuppressWarnings("rawtypes")
				@Mock
				private List mock ;
				@SuppressWarnings("rawtypes")
				@Mock
				private List list = new LinkedList();
				@Mock
				List spy = spy(list);


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
   public void  mockitoTest   (){
       
		
		
		

		
		
		//using mock object
		mockedList.add("one");
		mockedList.clear();

		//verification
		verify(mockedList).add("one");
		verify(mockedList).clear();


		//using mock 
		mockedList.add("once");

		mockedList.add("twice");
		mockedList.add("twice");

		mockedList.add("three times");
		mockedList.add("three times");
		mockedList.add("three times");

		//following two verifications work exactly the same - times(1) is used by default
		verify(mockedList).add("once");
		verify(mockedList, times(1)).add("once");

		//exact number of invocations verification
		verify(mockedList, times(2)).add("twice");
		verify(mockedList, times(3)).add("three times");

		//verification using never(). never() is an alias to times(0)
		verify(mockedList, never()).add("never happened");
		
//			//verification using atLeast()/atMost()
//			verify(mockedList, atLeastOnce()).add("three times");
//			verify(mockedList, atLeast(2)).add("five times");
//			verify(mockedList, atMost(5)).add("three times");
//

//doThrow(new Exception("Exception is thrown when we clear the list")).when(mockedList).clear();
try{ //cant catch the exception ?
		// but why to test if i want to catch it ==>  STUB !! 
//following throws RuntimeException:
mockedList.clear();
}catch(Exception e) {System.out.println("Cought exc so we r going forward");
}

System.out.println("ON !");
		

//Sometimes we need to stub with different return value/exception for the same method call. Typical use case could be mocking iterators.
//stubbing many objects in example for multiple variables
when(mock.get(0)).thenReturn("one", "two", "three");
for (int i=0; i < 6 ; i++){
	System.out.println("Tab["+i+"] = "+mock.get(0));//return another values from .thenReturn - last value is repeated for further requests of value
}

// You can use doThrow(), doAnswer(), doNothing(), doReturn() and doCallRealMethod()-calls a real method from the main object on the mocked object - comparison 
//	in place of the corresponding call with when(), for any method. It is necessary when you 



when(mock.get(0))
  .thenThrow(new RuntimeException())
  .thenReturn("foo");

//First call: throws runtime exception:		
		//added catch - can make it shorter with something like - http://stackoverflow.com/questions/156503/how-do-you-assert-that-a-certain-exception-is-thrown-in-junit4-5-tests
try {
	mock.get(0);
} catch (Exception e) { //defined above is the runtime exception
	System.out.println( "My method did throw when I expected it to" );
}

//Second call: prints "foo"
System.out.println(mock.get(0));

//Any consecutive call: prints "foo" as well (last stubbing wins). 
System.out.println(mock.get(0));

System.out.println("SPY part : ");
spy.add("foo");
//Impossible: real method is called so spy.get(0) throws IndexOutOfBoundsException (the list is yet empty)
when(spy.get(0)).thenReturn("foo");
//You have to use doReturn() for stubbing
System.out.println("doReturnOnSpy => " + spy.get(0) );

/*
Instead of reset() please consider writing simple, small and focused test methods over lengthy, over-specified tests. 
First potential code smell is reset() in the middle of the test method. This probably means you're testing too much. 
Follow the whisper of your test methods: "Please keep us small & focused on single behavior". There are several threads about it on mockito mailing list. 
List mock = mock(List.class);
when(mock.size()).thenReturn(10);
mock.add(1);
reset(mock);
//at this point the mock forgot any interactions & stubbing
*/

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Behavior Driven Development style of writing tests uses //given //when //then comments as fundamental parts of your test methods. This is exactly how we write our tests and we warmly encourage you to do so!
Start learning about BDD here: http://en.wikipedia.org/wiki/Behavior_Driven_Development 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


Mocks can be made serializable. With this feature you can use a mock in a place that requires dependencies to be serializable.
WARNING: This should be rarely used in unit testing. 

Verification with timeout
verify(mock, timeout(100)).someMethod();
03	//above is an alias to:
04	verify(mock, timeout(100).times(1)).someMethod();
*/
									//WHY IS IT FALSE ? ? ? 
list.add("foo");list.add("foo");
spy.add("foo");
System.out.println("doReturnOnSpy 2 => " + verify(spy, timeout(100).times(2)).add("foo") );
System.out.println("1 lne mocker");
//One-liner stubs -  
	//make the object with the mock
List stubbedSingleLineList = when(mock(List.class).add("Excep added")).thenThrow(Exception.class).getMock();
//add another example of testing using only WHEN with the created list 
when(stubbedSingleLineList.get(999)).thenThrow(Exception.class).getMock();

try{stubbedSingleLineList.add("Excep added");}catch(Exception e){	System.out.println("1 line stubb exception thrown");}
try{stubbedSingleLineList.get(999		   );}catch(Exception e){	System.out.println("1 line RunExc when we do get(999)");}

/*
 * Mockito.mockingDetails(someObject).isMock();
2	Mockito.mockingDetails(someObject).isSpy();
Both the MockingDetails.isMock() and MockingDetails.isSpy() methods return boolean. 
As a spy is just a different kind of mock, isMock() returns true if the object is a spy. 
In future Mockito versions MockingDetails may grow and provide other useful information about the mock, 
e.g. invocations, stubbing info, etc. 
 */




       

	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	// UTILS and short codes.
	
	/**
	 * Shortcode for changing the language by clicking the flag
	 */
	public void changeLocale(WebDriver driver, String eng){
	    WebElement sf = driver.findElement(By.id("lang_"+eng)  );
        sf.click();
	}
	
	
	/**
	 * For closing and quiting selenium / driver. 
	 * 	driver.close();
     *  driver.quit();
	 */
	public void Close(WebDriver driver){
		driver.close();
        driver.quit();
	}
	
	/**
	 * Login to system as user with specified role types (authorities)
	 * @param User
	 */
	   public void loginToSystem(WebDriverBackedSelenium sel){
		   System.out.println("loginToSystemStartMethod!! !! ");
	        sel.type("j_username",ulog) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);	}
			catch(InterruptedException ie){	}
	        sel.type("j_password",upass) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);}
			catch(InterruptedException ie){	}
	        sel.click("submit");
	        sel.waitForPageToLoad(timePageLoad);
	        System.out.println("after pageLoad ");
	
	   }
	
		
	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	   
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   

	 //setters getters 
	 	   
	 	    

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}




	public String getContext() {
		return context;
	}




	public void setContext(String context) {
		this.context = context;
	}




	public String getAdduserurl() {
		return adduserurl;
	}




	public void setAdduserurl(String adduserurl) {
		this.adduserurl = adduserurl;
	}




	public String getIndexurl() {
		return indexurl;
	}




	public void setIndexurl(String indexurl) {
		this.indexurl = indexurl;
	}




	public String getLogouturl() {
		return logouturl;
	}




	public void setLogouturl(String logouturl) {
		this.logouturl = logouturl;
	}









}








