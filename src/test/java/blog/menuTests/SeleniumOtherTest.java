package blog.menuTests;


import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 *	Bunch of test with selenium vor various purposes.,
 *check the method names and javadoc for more info
 *
  */


public class SeleniumOtherTest {

//constants for tests
	
				String context = "/Blog/";
				//String context = "/";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String adduserurl = "http://localhost:8080"+context+"register/reguser";
				String indexurl = "http://localhost:8080"+context;
				String logouturl = "http://127.0.0.1:8080"+context+"j_spring_security_logout";
					
				
				
				
				//To get codes for i18n testing.
				//gets all the codes from all the props
				private MessageSource msgSrc;  
				@Autowired  
				  public void AccountsController(MessageSource msgSrc) {  
				     this.setMsgSrc(msgSrc);  
				  }
				
				
				
	/**
	 * 
	 * 	Create user user, logut and log in with new credentials.
	 *  Assert with the greeting message for user .
	 */
	
	@SuppressWarnings("static-access")
	@Test
   public void  testCreateAndLoginUserTest   (){
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);        	
        loginToSystem(sel);
        sel.open(adduserurl);
//changing credentials to newly added user 
        this.ulog	= "user_"+getRandomString(4);
        this.upass  = "upass_"+getRandomString(4);
//loggin in with new user         		
        sel.type("login",ulog ) ;try{ Thread.currentThread().sleep(waitTime);	}
		catch(InterruptedException ie){	}
        sel.type("passwd",upass) ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}       
        sel.click("submit");
//go to logout 
        sel.open(logouturl);    
        loginToSystem(sel);
	
        WebElement searchField = driver.findElement(By.id("greetingUser")  );
        String str = searchField.getText();
     Assert.assertEquals("Login and greeting mesasge missmatch"	,	"Witaj\n"+ulog	,	str);
        
        
        driver.close();
        driver.quit();
       
    }
	
	
	
	/**
	 * 
	 * I18n test for checking example phrases and languages
	 * 
	 */		
	@Test
	public void testMainSiteAfterLoginGreetingMessage() {
		//arrays with phrases / ids and generate 
	        WebDriver driver = new FirefoxDriver();
	        driver.get(url);
	//check PL - default        
		//here we could get some awesome method acquiring all the elements by ids hesame as
		//i18n key name from prop files and we can test all together.
		//but who normal will ID every element according to what we have in language props files...
	Assert.assertEquals("I18n error at welcome"	,"Witaj",driver.findElement(By.id("welcomeMsg")).getText());
	//check ENG      
	changeLocale(driver, "eng");
	//String welcomePropMsg = msgSrc.getMessage("OK", null, pl);//locale idea ?
	Assert.assertEquals("I18n error at welcome"	,"Welcome"	,	driver.findElement(By.id("welcomeMsg")).getText());
	Close(driver);
}
	
	

	
	// UTILS and short codes.
	
	/**
	 * Shortcode for changing the language by clicking the flag
	 */
	public void changeLocale(WebDriver driver, String eng){
	    WebElement sf = driver.findElement(By.id("lang_"+eng)  );
        sf.click();
	}
	
	
	/**
	 * For closing and quiting selenium / driver. 
	 * 	driver.close();
     *  driver.quit();
	 */
	public void Close(WebDriver driver){
		driver.close();
        driver.quit();
	}
	
	/**
	 * Login to system as user with specified role types (authorities)
	 * @param User
	 */
	   public void loginToSystem(WebDriverBackedSelenium sel){
		   System.out.println("loginToSystemStartMethod!! !! ");
	        sel.type("j_username",ulog) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);	}
			catch(InterruptedException ie){	}
	        sel.type("j_password",upass) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);}
			catch(InterruptedException ie){	}
	        sel.click("submit");
	        sel.waitForPageToLoad(timePageLoad);
	        System.out.println("after pageLoad ");
	
	   }
	
		
	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	   
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   

	 //setters getters 
	 	   
	 	    

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}




	public String getContext() {
		return context;
	}




	public void setContext(String context) {
		this.context = context;
	}




	public String getAdduserurl() {
		return adduserurl;
	}




	public void setAdduserurl(String adduserurl) {
		this.adduserurl = adduserurl;
	}




	public String getIndexurl() {
		return indexurl;
	}




	public void setIndexurl(String indexurl) {
		this.indexurl = indexurl;
	}




	public String getLogouturl() {
		return logouturl;
	}




	public void setLogouturl(String logouturl) {
		this.logouturl = logouturl;
	}



	public MessageSource getMsgSrc() {
		return msgSrc;
	}



	public void setMsgSrc(MessageSource msgSrc) {
		this.msgSrc = msgSrc;
	}
	










}








