package blog.menuTests;


import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

 /**
  * Test for links and form from menu concerning posts - adding , deleting etc.
  * 
  * @author TalentLab1
  * TO DO : 
  * Make all test as components - run adding, deleting and editing from different methods and 
  * make them integration tests easier without copying the code 
  *
  */

public class SeleniumMenuAdminTest {

//constants for tests
	
				String context = "/Blog";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String addurl = "http://localhost:8080"+context+"/posts/add";
				String indexurl = "http://localhost:8080"+context;
	/**
	 * 
	 * 	Check the clickability of link in the main index page - made on top HttpClient
	 * 
	 */
	
	@SuppressWarnings("static-access")
	@Test
   public void testCheckAllLinksForAdminIfTheyAre200OK   (){
			WebDriver driver = new FirefoxDriver();
	        driver.get(url);
	        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, url);
	        loginToSystem(sel);
	        List<WebElement> searchField = driver.findElements(By.tagName("a"));
		   
		   for (int i=0 ; i < searchField.size() ; i++){
			   String link = searchField.get(i).getAttribute("href");
			   HttpClient client = new DefaultHttpClient();
			   HttpGet response = new HttpGet(link);
			   HttpResponse httpResp = null ;
			   //System.out.println("Link = "+link); System.out.println("Resp = "+response);
			   
			try {
				httpResp = client.execute(response);
				//System.out.println("httpResp = "+httpResp);

			} catch (ClientProtocolException e) {
				System.out.println(" ClientProtocolException ");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println(" IOException ");
				e.printStackTrace();
			}
			   int code = httpResp.getStatusLine().getStatusCode();
			   if (code == 200) {
				   System.out.print(" Dziala");
				   System.out.print(" CODE = " + code);
				   System.out.print(" GET = " + link+"\n");
			   }
			   else {
				   System.out.print(" Klops ");
				   System.out.print(" CODE = " + code);
				   System.out.print(" GET = " + link+"\n");
			   }
			   
			   Assert.assertEquals(200, code);
			   
		   }//end for 
		   
	        driver.close();
	        driver.quit();
	}	
	
	
	/**
	 * 
	 * Test the calculated outputs on the form after clicking the submit button. Without persisting the outcomes.
	 * 
	 */
		
	@SuppressWarnings({ "static-access" })
	@Test
	public void testMainSiteAfterLoginGreetingMessage() {
		System.out.println(" testMainSiteAfterLoginGreetingMessage !! !! ");
        WebDriver driver = new FirefoxDriver();
        driver.get(url);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, url);
        loginToSystem(sel);
		System.out.println("After loginToSystem !! !! ");
        //	Circle test
        WebElement searchField = driver.findElement(By.id("greetingUser")  );
        String str = searchField.getText();
        Assert.assertEquals("Login and greeting mesasge missmatch"	,	"Witaj\n"+ulog	,	str);
        driver.close();
        driver.quit();       
}

	/**
	 * 
	 * Test to check deleting functionality of posts for admin user.	<br />
	 * 
	 * Have to add a adding post function for reusability
	 * 
	 */
	  
	@SuppressWarnings({ "static-access" })
	@Test
	@Deprecated
	public void testPostDelete() {
	    WebDriver driver = new FirefoxDriver();
        driver.get(url);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, url);
        loginToSystem(sel);
        List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name("deleteLink")  );
        WebElement postsCountBefore = driver.findElement(By.id("postsCount"));
        int postsSizeBefore =  (int)	Integer.parseInt(  postsCountBefore.getText()  );
        System.out.println(" Number deleteLink`s element  =>  "+postsSizeBefore);
        editLinks.get(0).click();
        driver.switchTo().alert().accept();
        driver.switchTo().alert().accept();
        sel.waitForPageToLoad(timePageLoad);

        WebElement postsCountAfter = driver.findElement(By.id("postsCount"));
        int size2 =  (int)	Integer.parseInt(  postsCountAfter.getText()  );
        
Assert.assertEquals("Zla ilosc postow" , postsSizeBefore , size2+1 );
        
        driver.close();
        driver.quit();
       
    }
	
	
	
	/**
	 * 
	 * Test adding a post and checking the amount of posts on the page,		<BR />
	 * TO DO : use with delete tets to keep posts at the db unchanged
	 * 
	 */
	
	@SuppressWarnings({ "static-access" })
	@Test
	public void testPostAddingAndCountIndexPosts() {

		
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);

//count posts on login page 
        WebElement postsCountBefore = driver.findElement(By.id("postsCount"));
        int postsSizeBefore =  (int)	Integer.parseInt(  postsCountBefore.getText()  );
         	
        loginToSystem(sel);
//clicking to add post form        
        WebElement formLink =  (WebElement) driver.findElement(By.id("link2_addpost"));
        formLink.click();
        sel.waitForPageToLoad(timePageLoad);
//add post form
        sel.type("title","12345_TEST_"+getRandomString(4) ) ;try{ Thread.currentThread().sleep(waitTime);	}
		catch(InterruptedException ie){	}
        sel.type("content","123456789") ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}       
        WebElement we0 =  (WebElement) driver.findElement(By.id("persistCheckBox"));
        we0.click();
        WebElement we1 =  (WebElement) driver.findElement(By.id("publicPostCheckBox"));
        we1.click();
        
//save post        
        sel.click("submit");
        
//check msg for success 
        WebElement we = driver.findElement(By.className("allOK"));
        String okmsgget = we.getText();
        String okmsg = "Zapisano wpis";        
Assert.assertEquals("Post saved in db " , okmsg , okmsgget );
		
//get ID of the post added
		WebElement addedPostId = driver.findElement(By.id("postid"));
		int postid = Integer.parseInt(addedPostId.getText());
		System.out.println(" ID zapisanego postu = "+postid);
	
//go to index 
        sel.open(indexurl);
//check posts number
        //cant get when field in hidden - need to be display:none
       // JavascriptExecutor jse = (JavascriptExecutor)driver;
       // jse.executeScript("document.getElementsById('postsCount')[0].innerHTML ;");
		WebElement postsCount = driver.findElement(By.id("postsCount"));
        int postsSizeAfter =  (int)	Integer.parseInt(  postsCount.getText()  );

        System.out.println(" Posts number on index after adding = " + postsSizeAfter);
Assert.assertEquals("Number of posts OK " , postsSizeBefore+1 , postsSizeAfter );
        
//  Delete addes test posts - sel.open url to del ?
		WebElement deleteLink = driver.findElement(By.id("deleteLink"+postid));
		deleteLink.click();
        driver.switchTo().alert().accept();
        driver.switchTo().alert().accept();

System.out.println(" PostSizeBefore" + postsSizeBefore  );
System.out.println(" PostsSizeAfter" +  postsSizeAfter );

Assert.assertEquals("Number of posts after deleting OK" , postsSizeBefore , postsSizeAfter-1 );

        
        driver.close();
        driver.quit();
       
    }
	
	
	
	
	// UTILS and short codes.
	

	/**
	 * Login to system as user with specified role types (authorities)
	 * @param User
	 */
	   public void loginToSystem(WebDriverBackedSelenium sel){
		   System.out.println("loginToSystemStartMethod!! !! ");
	        sel.type("j_username",ulog) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);	}
			catch(InterruptedException ie){	}
	        sel.type("j_password",upass) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);}
			catch(InterruptedException ie){	}
	        sel.click("submit");
	        sel.waitForPageToLoad(timePageLoad);
	        System.out.println("after pageLoad ");
	
	   }
	
		/**
		 * Login to system start from initiating webdriver with index Url,		<br />
		 * after loggin we r at the main page, admin logging default			<br />
		 * @param User
		 */
		   public void loginToSystemMoreCode(String baseUrl){
			   	WebDriver driver = new FirefoxDriver();
		        driver.get(url);
		        WebDriverBackedSelenium sel1= new WebDriverBackedSelenium(driver, url);	
		        System.out.println("loginToSystemStartMethod!! !! ");
		        sel1.type("j_username",ulog) ;try{ Thread.currentThread();
				Thread.sleep(waitTime);	}
				catch(InterruptedException ie){	}
		        sel1.type("j_password",upass) ;try{ Thread.currentThread();
				Thread.sleep(waitTime);}
				catch(InterruptedException ie){	}
		        sel1.click("submit");
		        sel1.waitForPageToLoad(timePageLoad);
		        System.out.println("after pageLoad ");		
		   }
	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	   
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   

	 //setters getters 
	 	   
	 	    

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}
	










}








