package blog.menuTests;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Test;
import blog.entities.User;
import blog.hibernate.HashMachine;
import blog.hibernate.HibGetDBSession;
import blog.hibernate.UserDAO;

 /**
  * Tests for User DAO with specified hibernate cfg file for tests only. .
  * 
  * @author TalentLab1
  *
  */

public class UserDAOTest {
	
	String hibcfgxmlFile = "testhibernate.cfg.xml"; 
	
	/**
	 * Saving users tests with scutom hibernate file session.
	 */
	
@Test	
public void saveUserTestHibFileSessionTest(){	
	
	User user = new User();
	User dummyUser = new User();
	
	PostDAOTest pdt = new PostDAOTest();
	HashMachine hm = new HashMachine(); 
	
	user.setLogin(pdt.getRandomString(6));
	user.setPasswd( hm.hashThePass_USE_THIS("qwe"));
	HibGetDBSession fabrykaHibernejta = new HibGetDBSession();

	//save a user    	
	try {
   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibcfgxmlFile);
		Transaction tx = session.beginTransaction();
		//hsh
		session.save(user);
		tx.commit();
		System.out.println("User id = "+user.getId()+" added !");
	} catch (Exception e) {
		System.out.println("Error saving single users - \n ");
				e.printStackTrace();
	}
//load added user and delete it 
	try {
   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibcfgxmlFile);
		Transaction tx = session.beginTransaction();
		dummyUser = (User)session.get(User.class,user.getId());
		tx.commit();
 		Session session1 = fabrykaHibernejta.getNewSessionAnnotations(hibcfgxmlFile);
		Transaction tx1 = session1.beginTransaction();
		session1.delete(user);
		tx1.commit();
	} catch (Exception e) {
		System.out.println("Error saving single users - blog.config.DBConfig error.\n ");
		e.printStackTrace();
	} 	
//check toStrings methods   	
String test1 = user.getId() + user.getLogin() + user.getPasswd();
String test2 = dummyUser.getId() + dummyUser.getLogin() + dummyUser.getPasswd();
Assert.assertEquals(test1, test2); 	

}





@Test	
public void saveLoadDeleteUserTestDAO(){	
	
	User user = new User();
	User duser1 = new User();
	User duser2 = new User().getNewBlankUser();
	
	PostDAOTest pdt = new PostDAOTest();
	HashMachine hm = new HashMachine(); 
	UserDAO udao = new UserDAO();
	
	user.setLogin(pdt.getRandomString(6));
	user.setPasswd( 	hm.hashThePass_USE_THIS("qwe123")	);

	Session ses = udao.getFabrykaHibernejta().getNewSessionAnnotationsWithHibernateCfgXmlSession(hibcfgxmlFile);
	udao.getFabrykaHibernejta().setSession(  ses  );
	
	udao.saveUser(user);
	
	//duser1 = udao.getUserByID(user.getId());
	duser2 = udao.getUserByString(user.getLogin());
	
	Assert.assertEquals(user.toString(), duser2.toString()); 	
	
	//get user list and count the users 
	int userCountBeforeDelete = udao.getAllUsers().size();
	udao.deleteUser(user);
	int userCountAfterDelete = udao.getAllUsers().size();
	
	Assert.assertEquals(	userCountBeforeDelete-1 , userCountAfterDelete	);


}





}