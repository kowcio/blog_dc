package blog.menuTests;


import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

 /**
  * Test for links and form from menu concerning posts - adding , deleting etc.
  * 
  * @author TalentLab1
  * TO DO : 
  * Make all test as components - run adding, deleting and editing from different methods and 
  * make them integration tests easier without copying the code 
  *
  */

public class SeleniumNotepadControllerTest {

//constants for tests
	
				String context = "/Blog";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String addurl = "http://localhost:8080"+context+"/posts/add";
				String indexurl = "http://localhost:8080"+context;
	
	/**
	 * 
	 * Test of adding the post, checking it in notepad and publishing it.
	 * 
	 */
	
	@SuppressWarnings({ "static-access" })
	@Test
	public void addPostToNotepad_CheckTheAddedPost_MakeThePostPublic_CheckThePostPublic_DeletePost() {

//login in		
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);

        loginToSystem(sel);
        
//count posts on login page 
        WebElement postsCountBefore = driver.findElement(By.id("postsCount"));
        int postsSizeBefore =  (int)	Integer.parseInt(  postsCountBefore.getText()  );
         	
//go to add post form        
        WebElement formLink =  (WebElement) driver.findElement(By.id("link2_addpost"));
        formLink.click();
        sel.waitForPageToLoad(timePageLoad);
//add post form
        sel.type("title","12345_TEST_"+getRandomString(4) ) ;try{ Thread.currentThread().sleep(waitTime);	}
		catch(InterruptedException ie){	}
        sel.type("content","123456789") ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}       
        WebElement we0 =  (WebElement) driver.findElement(By.id("persistCheckBox"));
        we0.click();
        
//save post        
        sel.click("submit");
        
//check msg for success 
        WebElement we = driver.findElement(By.className("allOK"));
        String okmsgget = we.getText();
        String okmsg = "Zapisano wpis";        
Assert.assertEquals("Post saved in db " , okmsg , okmsgget );
		
//get ID of the post added
		WebElement addedPostId = driver.findElement(By.id("postid"));
		int postid = Integer.parseInt(addedPostId.getText());
		System.out.println(" ID zapisanego postu = "+postid);
		
//go to index 
        sel.open(indexurl);
//check posts number
        //cant get when field in hidden - need to be display:none
       // JavascriptExecutor jse = (JavascriptExecutor)driver;
       // jse.executeScript("document.getElementsById('postsCount')[0].innerHTML ;");
		WebElement postsCount = driver.findElement(By.id("postsCount"));
        int postsSizeAfter =  (int)	Integer.parseInt(  postsCount.getText()  );

Assert.assertEquals("Number of posts OK " , postsSizeBefore , postsSizeAfter );
        

//go to notepad
WebElement notepadLink =  (WebElement) driver.findElement(By.id("alink_notepad"));
notepadLink.click();
sel.waitForPageToLoad(timePageLoad);

//click the post Publish link
WebElement notepadPublicPostLink =  (WebElement) driver.findElement(By.id("publicPostLink"+postid));
notepadPublicPostLink.click();
sel.waitForPageToLoad(timePageLoad);

//go to index to check the published post
sel.open(indexurl);

//check the id for the post if it is present 
WebElement publishedPostCheck =  (WebElement) driver.findElement(By.id("showLink"+postid));
	if ( publishedPostCheck != null){
		Assert.assertEquals("publishedPostCheck != null => we got a value = post exists", 
			publishedPostCheck.getAttribute("id") , "showLink"+postid   );
	}
	else{
		Assert.assertTrue("publishedPostCheck == null ",	false  );
	}
		

//  Delete addes test posts - sel.open url to del ?
		WebElement deleteLink = driver.findElement(By.id("deleteLink"+postid));
		deleteLink.click();
        driver.switchTo().alert().accept();
        driver.switchTo().alert().accept();
        driver.close();
        driver.quit();
       
    }
	
	
	
	/**
	 * 
	 * Add post to notepda and check the edit checkBox for making the post public.
	 * 
	 */
	
	@SuppressWarnings({ "static-access" })
	@Test
	public void addPostToNotepad_MakeThePostPublicWithCheckBox_CheckThePostPublic_DeletePost() {

//login in		
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);

        loginToSystem(sel);
        int postid = addPostToNotepadAndGoBackToIndex(sel,driver);
        
        sel.click("alink_notepad");
        sel.click("userEditPostLink"+postid);
        sel.click("publicPostCheckBox");
        sel.click("submit");
        
        sel.open(indexurl);

      //check the id for the post if it is present 
        WebElement publishedPostCheck =  (WebElement) driver.findElement(By.id("showLink"+postid));
        	if ( publishedPostCheck != null){
        		Assert.assertEquals("publishedPostCheck != null => we got a value = post exists", 
        			publishedPostCheck.getAttribute("id") , "showLink"+postid   );
        	}
        	else{
        		Assert.assertTrue("publishedPostCheck == null ",	false  );
        	}
        
        deleteElementByIDAndQuitDriver(driver, postid); 	
        	
	}
	
	
	
	
	
	
	
	// UTILS and short codes.
	
	/**
	 * Login to system as user with specified role types (authorities)
	 * @param User
	 */
	   public void loginToSystem(WebDriverBackedSelenium sel){
		   
		   System.out.println("loginToSystemStartMethod!! !! ");
	        sel.type("j_username",ulog) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);	}
			catch(InterruptedException ie){	}
	        sel.type("j_password",upass) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);}
			catch(InterruptedException ie){	}
	        sel.click("submit");
	        sel.waitForPageToLoad(timePageLoad);
	        System.out.println("after pageLoad ");
	   }

	   /**
	    * method For easier clicking but we can just use
	    * sel.click("id srting ");
	    * @param driver
	    * @param name
	    */
	   public void clickTheElementById(WebDriver driver, String name){
	       WebElement we1 =  (WebElement) driver.findElement(By.id(name));
	       we1.click();
	   }
	   
	   /**
	    * Delete the post by given id, close and quit the driver.		<br />
	    * clicked on link 	"deleteLink"+postid							<br />
	    * @param driver
	    * @param postid
	    */
	   public void deleteElementByIDAndQuitDriver(WebDriver driver, int postid ){
	        //  Delete addes test posts - sel.open url to del ?
	    		WebElement deleteLink = driver.findElement(By.id("deleteLink"+postid));
	    		deleteLink.click();
	            driver.switchTo().alert().accept();
	            driver.switchTo().alert().accept();
	            driver.close();
	            driver.quit();
	   }
	   
	   /**
	    * Add post to notepad and go to the index
	    */
public int addPostToNotepadAndGoBackToIndex( WebDriverBackedSelenium sel, WebDriver driver){	   
	   WebElement formLink =  (WebElement) driver.findElement(By.id("link2_addpost"));
       formLink.click();
       sel.waitForPageToLoad(timePageLoad);
//add post form
       sel.type("title","12345_TEST_"+getRandomString(4) ) ;try{ Thread.currentThread().sleep(waitTime);	}
		catch(InterruptedException ie){	}
       sel.type("content","123456789") ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}       
       WebElement we0 =  (WebElement) driver.findElement(By.id("persistCheckBox"));
       we0.click();
//save post        
       sel.click("submit");
//check msg for success 
       WebElement we = driver.findElement(By.className("allOK"));
       String okmsgget = we.getText();
       String okmsg = "Zapisano wpis";        
       Assert.assertEquals("Post saved in db " , okmsg , okmsgget );
//get ID of the post added
		WebElement addedPostId = driver.findElement(By.id("postid"));
		int postid = Integer.parseInt(addedPostId.getText());
		System.out.println(" ID zapisanego postu = "+postid);
//go to index 
       sel.open(indexurl);
	   return postid;
}
	   
	   
	   	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	   
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   

	 //setters getters 
	 	   
	 	    

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}
	










}








