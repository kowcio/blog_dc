package blog.menuTests;


import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

 /**
  * Test for links and form from menu concerning posts - adding , deleting etc.
  * @author TalentLab1
  * TO DO : 
  * Make all test as components - run adding, deleting and editing from different methods and 
  * make them integration tests easier without copying the code 
  *
  */
public class SeleniumCommentsTest {
//constants for tests
				String context = "/Blog";
	   			String url = "http://localhost:8080"+context;	//root with form to login or main index page
				int waitTime 		=  250;					//ms for input fields 
				String timePageLoad = "2500";				//ms
				String ulog = "admin";						//user for tests login
				String upass = "admin";						//user for tests password
				String addurl = "http://localhost:8080"+context;
				String indexurl = "http://localhost:8080"+context;

	/**
	 * 
	 * Add the comment as anonymous user, approve it as admin and check if it 
	 * shows in the main view when nobody is logged in.
	 * 
	 */
	
	@SuppressWarnings({ "static-access" })
	@Test
	public void testPostAddingAndCountIndexPosts() {

		
        WebDriver driver = new FirefoxDriver();
        driver.get(indexurl);
        WebDriverBackedSelenium sel= new WebDriverBackedSelenium(driver, indexurl);

//count posts on login page 
        WebElement postsCountBefore = driver.findElement(By.id("postsCount"));
        int postsSizeBefore =  (int)	Integer.parseInt(  postsCountBefore.getText()  );
         	
        loginToSystem(sel);
//clicking to add post form        
        WebElement formLink =  (WebElement) driver.findElement(By.id("link2_addpost"));
        formLink.click();
        sel.waitForPageToLoad(timePageLoad);
//add post form
        sel.type("title","12345_TEST_"+getRandomString(4) ) ;try{ Thread.currentThread().sleep(waitTime);	}
		catch(InterruptedException ie){	}
        sel.type("content","123456789") ;try{ Thread.currentThread().sleep(waitTime);}
		catch(InterruptedException ie){	}       
        WebElement we0 =  (WebElement) driver.findElement(By.id("persistCheckBox"));
        we0.click();
        WebElement we1 =  (WebElement) driver.findElement(By.id("publicPostCheckBox"));
        we1.click();
        
//save post        
        sel.click("submit");
        
//check msg for success 
        WebElement we = driver.findElement(By.className("allOK"));
        String okmsgget = we.getText();
        String okmsg = "Zapisano wpis";        
Assert.assertEquals("Post saved in db " , okmsg , okmsgget );
		
//get ID of the post added
		WebElement addedPostId = driver.findElement(By.id("postid"));
		int postid = Integer.parseInt(addedPostId.getText());
		System.out.println(" ID zapisanego postu = "+postid);
	
//go to index 
        sel.open(indexurl);
//check posts number
        //cant get when field in hidden - need to be display:none
       // JavascriptExecutor jse = (JavascriptExecutor)driver;
       // jse.executeScript("document.getElementsById('postsCount')[0].innerHTML ;");
		WebElement postsCount = driver.findElement(By.id("postsCount"));
        int postsSizeAfter =  (int)	Integer.parseInt(  postsCount.getText()  );

        System.out.println(" Posts number on index after adding = " + postsSizeAfter);
Assert.assertEquals("Number of posts OK " , postsSizeBefore+1 , postsSizeAfter );
        
//  Delete addes test posts - sel.open url to del ?
		WebElement deleteLink = driver.findElement(By.id("deleteLink"+postid));
		deleteLink.click();
        driver.switchTo().alert().accept();
        driver.switchTo().alert().accept();

System.out.println(" PostSizeBefore" + postsSizeBefore  );
System.out.println(" PostsSizeAfter" +  postsSizeAfter );

Assert.assertEquals("Number of posts after deleting OK" , postsSizeBefore , postsSizeAfter-1 );

        
        driver.close();
        driver.quit();
       
    }
	
	
	
	
	// UTILS and short codes.
	

	/**
	 * Login to system as user with specified role types (authorities)
	 * @param User
	 */
	   public void loginToSystem(WebDriverBackedSelenium sel){
		   System.out.println("loginToSystemStartMethod!! !! ");
	        sel.type("j_username",ulog) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);	}
			catch(InterruptedException ie){	}
	        sel.type("j_password",upass) ;try{ Thread.currentThread();
			Thread.sleep(waitTime);}
			catch(InterruptedException ie){	}
	        sel.click("submit");
	        sel.waitForPageToLoad(timePageLoad);
	        System.out.println("after pageLoad ");
	
	   }
	
		/**
		 * Login to system start from initiating webdriver with index Url,		<br />
		 * after loggin we r at the main page, admin logging default			<br />
		 * @param User
		 */
		   public void loginToSystemMoreCode(String baseUrl){
			   	WebDriver driver = new FirefoxDriver();
		        driver.get(url);
		        WebDriverBackedSelenium sel1= new WebDriverBackedSelenium(driver, url);	
		        System.out.println("loginToSystemStartMethod!! !! ");
		        sel1.type("j_username",ulog) ;try{ Thread.currentThread();
				Thread.sleep(waitTime);	}
				catch(InterruptedException ie){	}
		        sel1.type("j_password",upass) ;try{ Thread.currentThread();
				Thread.sleep(waitTime);}
				catch(InterruptedException ie){	}
		        sel1.click("submit");
		        sel1.waitForPageToLoad(timePageLoad);
		        System.out.println("after pageLoad ");		
		   }
	
	/**
	 * Get elements count by name of the field.
	 * @param driver
	 * @param name
	 * @return
	 */
	   public int getElementsCountByName(WebDriver driver, String name ){
		   List <WebElement> editLinks = (List<WebElement>) driver.findElements(By.name(name)  );
		   System.out.println(" Number of  '"+name+"' elements = "+editLinks.size());
       		return editLinks.size(); 
	   }
	   
	   /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString(int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	   

	 //setters getters 
	 	   
	 	    

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public int getWaitTime() {
		return waitTime;
	}


	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}


	public String getTimePageLoad() {
		return timePageLoad;
	}


	public void setTimePageLoad(String timePageLoad) {
		this.timePageLoad = timePageLoad;
	}


	public String getUlog() {
		return ulog;
	}


	public void setUlog(String ulog) {
		this.ulog = ulog;
	}


	public String getUpass() {
		return upass;
	}


	public void setUpass(String upass) {
		this.upass = upass;
	}
	










}








