package blog.controllers;

import java.util.Date;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import blog.entities.Post;
import blog.hibernate.PostDAO;

/**
 * Class made for fun checking out how to add ajax behaviour to a spring application, works awesome with jquery 
 * @author TalentLab1
 *
 */
@SuppressWarnings("unused")
@Controller
public class AjaxElementsController {

	
	/**
	 * Shows the data for ajax test field
	 * @param postNo
	 * @return
	 */
	@RequestMapping(value="/testajax",method= RequestMethod.GET , produces="text/html") //produces="text/html" does not do the job
	public @ResponseBody String testAjax(
			String ajaxTestString
			) {
		ajaxTestString = "<b>"+getRandomString(6)+"</b>"; 
		return  ajaxTestString;
	}
	
	
	 /**
	    * 	Generate random string with given length for title(unique) testing
	    * 
	    */
	   public String getRandomString( int len){
		   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   Random rnd = new Random();
	      StringBuilder sb = new StringBuilder( len );
		      for( int i = 0; i < len; i++ ) 
		         sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		
		      String str = sb.toString();
		      System.out.println(" generated string " +sb.toString() );
		      return str;
			   }

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
}
