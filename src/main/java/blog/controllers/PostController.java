package blog.controllers;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import blog.entities.Post;
import blog.entities.User;
import blog.hibernate.PostDAO;
import blog.hibernate.UserDAO;

/**
 * Controller for /posts/ urls
 * 
 * @author TalentLab1
 *
 */

@Controller
public class PostController {
	
	
	Logger logger = Logger.getLogger("Iter4Logger");
	Utils utl = new Utils();
    private MessageSource msgSrc;  
    @Autowired  
	  public void AccountsController(MessageSource msgSrc) {  
	     this.msgSrc = msgSrc;  
	  } 
	

	@RequestMapping(value="/posts")
	public ModelAndView showIndex(
			PostDAO dao,
			ModelAndView mav
			) {
		System.out.println(" Loading /posts");
		List<Post> pl = new ArrayList<>();
		
		mav.setViewName("/posts/show");
		pl = dao.getAllPosts();
		if (pl == null){ 
			 mav.addObject("postEMPTY" , "postListIsNull");
		}
		else {
			//loading the comments for the posts 
			loadCommentsForPostsList(pl);
			mav.addObject("PostsList" ,  pl);
		}
		return mav;
	}

	
	
	
	
	
	@RequestMapping(value="/posts/add",method= RequestMethod.GET)
	public ModelAndView postAddGETForm(
			@ModelAttribute("post") @Valid Post newPost, 
			ModelAndView mav
			) {
			mav.setViewName("/posts/add");
			return mav;
	}
	
	@RequestMapping(value="/posts/add",method= RequestMethod.POST)
	public ModelAndView postAddPOSTReturnResult(
			@ModelAttribute("post") @Valid Post newPost, 
			BindingResult result,
			ModelAndView mav,
			HttpServletRequest req,
    		PostDAO dao,
			UserDAO udao,
			User us,
			Utils utl
			) {

		   System.out.println("Errors : " + result.getAllErrors().toString());
		   if (result.hasErrors()){
			    mav.setViewName("/posts/add");
	        	System.out.println("We have errors here ...");
		  		mav.addObject( "saveStatus"	,	"input_error");
		  		mav.addObject("result", result);
	 			return mav;
		   }
		   else
		   {
	        	System.out.println("No errors  ...");
			   //zapisz do bazy
	    		if (req.getParameter("persist")!=null){
	    			int vT =  req.getParameter("public")== null ? 0 : 1 ;
	 	        	newPost.setViewType(	vT	);
	    				//saving the user ID to the post object 
	    				 us = udao.getUserByString( utl.getLoggedUsername().get(0)	);
	    				newPost.setUserID(  safeLongToInt(us.getId() )          );
	    		  		//System.out.println("Persisting to database ...");
	    					 newPost.getAddDate();
	    					 int postid = dao.savePost(newPost);
	    					 logger.info("User "+utl.getLoggedUsername().get(0)+"added post ID = "+newPost.getId() );
	    		    		if ( postid>0){
	    	        			mav.addObject( "saveStatus"	,	"save_ok");
	    	        		}
							else{
								mav.addObject( "saveStatus"	,	"save_notok");
							}
	    	        		//adding a status global from properties file
	    	        		//mav.addObject("status" ,msgSrc.getMessage("OK", null, null));
	    		}
			    mav.setViewName("/posts/add");
	        	mav.addObject("post",newPost);
	 			mav.addObject("result", result);
	 			return mav;
			    
		   }
		   
		   
		   
	}
	
	
	
	@RequestMapping(value="/posts/show/{postNo}",method= RequestMethod.GET)
	public ModelAndView postShowAll(
			@PathVariable("postNo") String postNo,
			PostDAO dao,
			ModelAndView mav,
			Post post
			) {
		System.out.println(" Loading post"+postNo);
		mav.setViewName("/posts/postNo");
		Integer postNoInt = Integer.valueOf(postNo);
		post = dao.getPostByID(postNoInt);
		if (post == null)
		{
			mav.addObject("postEMPTY" , "pos with given id = "+postNo +" does not exist in databse");
			mav.addObject("post" , post = Post.getNewPost("Brak", "Brak") );
		}
		else 
		{
			mav.addObject("post" , post );
		}
		return mav;
	}
	
	/**
	 * Method for deleting a post with given ID (only admin)
	 * @param postNo
	 * @return
	 */
	
	@RequestMapping(value="/posts/delete/{postNo}",method= RequestMethod.GET)
	public String postDeleteById(
			@PathVariable("postNo") String postNo
			) {
		System.out.println(" Loading post to delete = "+postNo);
		PostDAO dao = new PostDAO();
		Integer postNoInt = Integer.valueOf(postNo);
		dao.deletePost(postNoInt);
		logger.info("User "+utl.getLoggedUsername().get(0)+"deleted post ID = "+postNoInt );
		return "redirect:/";
		
	}
	
	
	
	
	
	
	
	
	
	
	
	//utilities and etc 
	
	/**
	 * Method for casting Long to INT
	 * @param l long
	 * @return int l
	 */
	public static int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	
	/**
	 * method loading comments for posts list
	 * @param lp
	 */
	public void loadCommentsForPostsList(List<Post> lp){
		for (int i=0 ; i < lp.size() ; i++){
			lp.get(i).loadPublicComments();
		}
	}
	
	
}