package blog.controllers;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import blog.entities.Post;
import blog.hibernate.PostDAO;
/**
 * Admin controller.
 * @author TalentLab1
 *
 */
@Controller
public class AdminController {

	Logger logger = Logger.getLogger("Iter4Logger");
	Utils utl = new Utils();

	
	
	
	/**
	 * Main index for admin is moved tothe root url "/" of application where the jps`s views includes 		<br />
	 * generate content from the authorities roles of logged user / non logged user.						<br />		
	 * Don`t use this url.																					<br />	
	 * 
	 * @return
	 */
	//@Deprecated
	@RequestMapping(value="/adminPanel",method= RequestMethod.GET)
	public ModelAndView indexForAdmin() {
		System.out.println(" Loading Admin panel");
		ModelAndView mav = new ModelAndView("/admin/adminPanel");
		
		//load posts list
		PostDAO dao = new PostDAO();
		if (dao.getAllPosts() == null){
			mav.addObject("postEMPTY" , "postListIsNull");
			System.out.println("PostList empty");
		}
		else {
			mav.addObject("PostsList" , dao.getAllPosts() );
		}
		return mav;

	}
	
	
	/**
	 * Shows the given post 
	 * @param postNo
	 * @return
	 */
	@RequestMapping(value="/adminPanel/post/{postNo}",method= RequestMethod.GET)
	public ModelAndView showEditFormToUpdatePost(
			@PathVariable("postNo") int postNo,
			PostDAO dao,
			Post post,
			ModelAndView mav
			) {
		System.out.println(" Loading edit of post"+postNo);
		mav.setViewName("/admin/editPostNo");
		post = dao.getPostByID(postNo);
		if (post == null)
		{
			mav.addObject("postEMPTY" , "pos with given id = "+postNo +" does not exist in databse");
			mav.addObject("post" , post = Post.getNewPost("Brak", "Brak") );
		}
		else mav.addObject("post" , post );
		
		return mav;
		
	}
	
	/**
	 * Posts edit form for admin user, the old value can be see after editing on the same view.
	 * @param postNo
	 * @return
	 */
	@RequestMapping(value="/adminPanel/edit/{postNO}",method= RequestMethod.POST)
	public ModelAndView saveorUpdateTheEditedPostByAdmin(
			@ModelAttribute("post") @Valid Post updatedPost, 
			BindingResult result,
			ModelAndView mav,
			HttpServletRequest req,
			PostDAO dao,
			HttpSession htses
			) {
		 System.out.println(" Logic when posting the post");
		 mav .setViewName("redirect:/");
		 int vT =  req.getParameter("public")== null ? 0 : 1 ; 
	
		mav.addObject("oldpost" , dao.getPostByID(updatedPost.getId()));

		  if (result.hasErrors()){
			  mav.addObject("allNotOK" , "post id = "+updatedPost.getId() +" update had an error");
			  return mav;
		  }
		  else
		  {
					try {
						if (updatedPost.getViewType()==1)
						{
							updatedPost.setViewType(1);
						}
						else {
							updatedPost.setViewType(vT);
						}

						
			  			
						dao.updatePost(updatedPost);
						logger.info("User "+utl.getLoggedUsername().get(0)+"edited post ID = "+updatedPost.getId() );
						mav.addObject("post" , updatedPost  );
						mav.addObject("allOK" , "post id = "+updatedPost.getId() +" update OK !!");
						mav.addObject("redir" ,"<meta http-equiv=\"Refresh\" content=\"4; url=http://localhost:8080/Blog/adminPanel\"/>" );
						
					} catch (Exception e) {
						mav.addObject("allNotOK" , "post id = "+updatedPost.getId() +" update had an error");
						e.printStackTrace();
					}
		  }
	
		
		return mav;
		
	}
	
	
	
}