package blog.controllers;


import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import blog.entities.Comment;
import blog.entities.Post;
import blog.hibernate.CommentDAO;
import blog.hibernate.PostDAO;

/**
 * Controller for /posts/ urls
 * @author TalentLab1
 */

@Controller
public class ComentsController {
	
	Utils utl = new Utils();
    
	/**
	 * View showing the comments
	 * @return
	 */
	@RequestMapping(value="/comments",method= RequestMethod.GET )
	public ModelAndView showCommentsForAdminToApprove(
			ModelAndView mav,
			CommentDAO cdao
			) {
		//LOGIC 
		mav.setViewName("posts/comments");		
		List<Comment> cl = cdao.getAllNotepadComments(0);
		//variables added to MAV
		if (cl == null || cl.isEmpty()) {
			System.out.println("comments list null or empty");
			mav.addObject("commentsEMPTY","commentsEMPTY");	
		}
		else {System.out.println("not null");
			mav.addObject("commentsList",cl);
		}
		return mav;
}
	
	
	
	/**
	 * View showing the comments
	 * @return
	 */
	@RequestMapping(value="/comment/public/{commNo}",method= RequestMethod.GET )
	public ModelAndView approveTheCommentWithID(
			@PathVariable("commNo") String commNo,
			ModelAndView mav,
			CommentDAO cdao,
			Comment comm
			) {
		int comid = Integer.parseInt(commNo);
		comm = cdao.getCommentByID(comid);
		System.out.println("comment viewType = "+comm.getViewType());
		comm.setViewType(1);
		cdao.updateComment(comm);
		System.out.println("comment viewType = "+comm.getViewType());
		mav.setViewName("redirect:/comments");

		return mav;
}
	
	/**
	 *Form to add a comment
	 * @return
	 */
	@RequestMapping(value={"/comment/add/{postID}"},method= RequestMethod.GET )
	public ModelAndView loadformToAddCommentToThePost(
			@ModelAttribute("comment") @Valid Comment newComment, 
			@PathVariable("postID") String postID,
			ModelAndView mav,
			PostDAO pdao
			) {
//set the comment		
		int postid = Integer.parseInt(postID);
		newComment.setPostID(postid);
		mav.setViewName("posts/addComment");
//set the post		
		mav.addObject("post", pdao.getPostByID(postid) );
		return mav;
}
	
	
	
	/**
	 *Logic validating and savingthe post.
	 * @return
	 */
	@RequestMapping(value="/comment/add/{postID}",method= RequestMethod.POST )
	public ModelAndView addCommentToThePostAndDatabasePOSTMethod(
			@PathVariable("postID") String postID,
			@ModelAttribute("comment") @Valid Comment newComment,
			BindingResult result,
			ModelAndView mav,
			CommentDAO cdao
			) {
//save the comment
		System.out.println("adding comment");
		
		 if (result.hasErrors())
		 {
		    mav.setViewName("redirect:posts/addComment"+newComment.getPostID());
		  	mav.addObject("commentToValidate", newComment);
		  	return mav;
		 }
		 else
		 {
		 	cdao.saveComment(newComment)	;
		 	mav.setViewName("redirect:/");
		 }
		
		 	return mav;

}
	
	
	
	
	
	
	
	//utilities and etc 
	
	/**
	 * Method for casting Long to INT
	 * @param l long
	 * @return int l
	 */
	public int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l +
	          " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	
	
	
	
}