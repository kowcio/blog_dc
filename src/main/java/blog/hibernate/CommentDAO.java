package blog.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import blog.controllers.Utils;
import blog.entities.Comment;

public class CommentDAO {

	

	Logger logger = Logger.getLogger("Iter4Logger");
	Utils utils = new Utils();
	//default hibernate cfg file for this DAO
	
	String hibfile = "hibernate.cfg.xml"; 
     
	
		/**
	 *Save a Comment class  into the user table
	 *
	 * @param  Comment comment  
	 * @return Saved comment ID allocated by hibernate autoincrement.
	 */
	
	public int saveComment(Comment comment){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	   	try {
	   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			System.out.println("Saving Record");
			Transaction tx = session.beginTransaction();
			comment.toString();
			session.save(comment);
			tx.commit();
			return comment.getId();
		} catch (Exception e) {
			System.out.println("Exception error - Saving Comment ");
			 e.printStackTrace();
			 return 0;
		}

	}   	
	
	
	
	/**
	 * @return All comments as list.
	 */
	@SuppressWarnings("unchecked")
	public List<Comment> getAllComments(){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		List<Comment> commentsList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			commentsList = (List<Comment>)session.createQuery("from Comment where viewType=1").list();
		   	System.out.println("CommentsList count = "+commentsList.size());	
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Exception error - CommentDAO - getAllComments");
			 e.printStackTrace();
		}
	   	return commentsList;
	}

	
	/**
	 * 	 * @return All comments in notepad for the user as list.
	 */
	
	
	@SuppressWarnings("unchecked")
	public List<Comment> getAllNotepadComments(int viewType){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		List<Comment> commentsList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			commentsList = (List<Comment>)session.createQuery("from Comment where viewType="+viewType).list();
		   	System.out.println("CommentsList count = "+commentsList.size());	
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Exception error - CommentDAO - getAllComments");
			 e.printStackTrace();
		}
	   	return commentsList;
	}
	
	/**
	 * Load 10 comments from given number
	 * @param numser of first comment
	 * @param number of comments to load
	 * @return Comments by 10 comments starting with given number.
	 */
	@SuppressWarnings("unchecked")
	public List<Comment> getAllCommentsBy10ForPaginationAjaxRequest(
			int firstComment,
			int numberofCommentsToLoad
			){
	   	System.out.println("PL_frogetAllCommentsBy10ForPaginationAjaxRequest");

		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();			 
			//count the comments
			//Long commentCount = (Long) session.createCriteria(Comment.class).setProjection(Projections.rowCount()).uniqueResult();
			
			String strQry = "from Comment c where viewType=1 order by c.id desc ";
		        Query query = session.createQuery(strQry);
		        query.setFirstResult(firstComment);
		        query.setMaxResults(numberofCommentsToLoad);
		    List<Comment> commentsList = query.list();   
		   	//System.out.println("CommentsList count 1 = "+commentCount +"\n q.list"+query.list());
		   	tx.commit();
		   	System.out.println("PL_fromDAO = "+commentsList.size());
		   	return commentsList;
	   	} catch (Exception e) {
			System.out.println("Exception error with loading ajax comments");
			 e.printStackTrace();
		}
	   	return new ArrayList<>();
	}

	
	
	/**
	 * Get comments count in db by selected viewType		</br > 
	 * 0 - notepads for all								</br>
	 * 1 - public for all 								</br>
	 * used reateCriteria and  Restrictions.eq("viewType", viewType ) </br >
	 * @return commentCount (long casted to int)
	 */
	@SuppressWarnings("unchecked")
	public int getCommentsNumberInDB(int viewType){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		List<Comment> commentsList = null;
		int pc = 0;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			//commentCount 
			int commentCount = 
					utils.safeLongToInt(
					(long) session.createCriteria(Comment.class)
					.add( Restrictions.eq("viewType", viewType ) )
					.setProjection(Projections.rowCount()).uniqueResult()
					);
			return commentCount;
	   	} catch (Exception e) {
			System.out.println("Exception error - CommentDAO - getCommentsNumberInDB");
			 e.printStackTrace();
		}
	   	return 0;
	}
	/**
	 * Return Comment by ID.
	 * 
	 * @param commentId - Comments ID which we wish to load.
	 * @return	Specified Comment
	 */
	
	
	public Comment getCommentByID(int commentId){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		Comment comment = new Comment();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			comment = (Comment) session.get(Comment.class, commentId);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Exception error - getCommentByID");
			 e.printStackTrace();
		}
	   
		System.out.println("Got comment, id = "+commentId);
	   	return comment;
	}

/**	Update a Comment object in db.						<br />
 * added throws RuntimeException for testing 		<br />
 * @param comment Comment which we wish to update. Needs to have a specified ID.<br />	
 */

	public void updateComment(Comment comment) throws RuntimeException{
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			session.update(comment);
			tx.commit();
			//Logging				
			//List<String> ut = new Utils().getLoggedUsername();
			//logger.info("User --"+ut.get(0)+"-- updated POST ID = "+comment.getId());//after this data dispolays in lower line , why ?
			
	   	} catch (Exception e) {
			System.out.println("Exception - getCommentByID");
			 e.printStackTrace();
		}
		System.out.println("Update comment = "+comment.toString());
	}


	/**	Delete in db a Comment object by given ID.
	 * 
	 * @param Needs to have a specified ID.
	 */

		public void deleteComment(int id){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		   	try {
				Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
				CommentDAO comment = new CommentDAO();
				Transaction tx = session.beginTransaction();
		   		System.out.println("Before delete");
		   		session.delete(comment.getCommentByID(id));
		   		System.out.println("After delete");
				tx.commit();
				//Logging				
				//List<String> ut = new Utils().getLoggedUsername();
				//logger.info("User --"+ut.get(0)+"-- deleted POST ID = "+id);//after this data dispolays in lower line , why ?
				
		   	} catch (IllegalArgumentException e) {  
		   		System.out.println(" No comment by that id");
				 e.printStackTrace();
	        }  	catch (Exception e) {
				System.out.println("Exception - DeleteCommentByID");
			}
		}


	
		/**
		 * Method used in DBConfig for saving the Comment object with provided opened session (taken from sessionFactory)
		 * @param comment
		 * @param sessionProvided
		 * @return "OK" or "NotOK"
		 */
		
		public int saveComment(Comment comment, Session sessionProvided){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		   	try {
		   		Session session = sessionProvided;
				Transaction tx = session.beginTransaction();
				comment.toString();
				session.save(comment);
				tx.commit();
				//logging
				//List<String> ut = new Utils().getLoggedUsername();
				//logger.info("User --"+ut.get(0)+"-- saved POST ID = "+comment.getId());//after this data dispolays in lower line , why ?
				
				System.out.println("Comment id = "+comment.getId()+" edited !");
				return comment.getId();
			} catch (Exception e) {
				System.out.println("Error saving single comments - blog.config.DBConfig error. ");
				 e.printStackTrace();
					return 0;
			}

		}
		
		
		
		
		
		/**
		 * 	@param id - int post id 
		 *  @param viewType 0 = public posts, 1 = to be approved
		 *  
		 *  @return All coments for given post - by post id 
		 */
		
		
		@SuppressWarnings("unchecked")
		public List<Comment> getAllCommentsForThePost(int postid, int viewType){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
			List<Comment> commentsList = null;
		   	try {
				Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
				Transaction tx = session.beginTransaction();
				commentsList = (List<Comment>)
						session.createQuery("from Comment where postid="+postid+"and viewType="+viewType).list();
			   	System.out.println("CommentsList count = "+commentsList.size());	
				tx.commit();
		   	} catch (Exception e) {
				System.out.println("Exception error - CommentDAO - getAllCommentsForThePost");
				 e.printStackTrace();
			}
		   	return commentsList;
		}
		
		
		
		
		

//setters getters

		public Logger getLogger() {
			return logger;
		}



		public void setLogger(Logger logger) {
			this.logger = logger;
		}



		public Utils getUtils() {
			return utils;
		}



		public void setUtils(Utils utils) {
			this.utils = utils;
		}



		public String getHibfile() {
			return hibfile;
		}



		public void setHibfile(String hibfile) {
			this.hibfile = hibfile;
		}
	
		
		
		
	

}
