package blog.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import blog.controllers.Utils;
import blog.entities.Post;

public class PostDAO {

	

	Logger logger = Logger.getLogger("Iter4Logger");
	Utils utils = new Utils();
	//default hibernate cfg file for this DAO
	
	String hibfile = "hibernate.cfg.xml"; 
     
	
		/**
	 *Save a Post class  into the user table
	 *
	 * @param  Post post  
	 * @return Saved post ID allocated by hibernate autoincrement.
	 */
	
	public int savePost(Post post){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	   	try {
	   		Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
	   		System.out.println("");
			System.out.println("Saving Record");
			Transaction tx = session.beginTransaction();
			post.toString();

			//setting the date of adding the post
			post.saveCreationEditDate();
			session.save(post);
			tx.commit();
			//logging
			//List<String> ut = new Utils().getLoggedUsername();
			//logger.info("User --"+ut.get(0)+"-- added POST ID = "+post.getId());//after this data dispolays in lower line , why ?
			
			return post.getId();
		} catch (Exception e) {
			System.out.println("Exception error - Saving Post ");
			 e.printStackTrace();
			 return 0;
		}

	}   	
	
	
	
	/**
	 * @return All posts as list.
	 */
	@SuppressWarnings("unchecked")
	public List<Post> getAllPosts(){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		List<Post> postsList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			postsList = (List<Post>)session.createQuery("from Post where viewType=1").list();
			System.out.println("PostsList count = "+postsList.size());	
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Exception error - PostDAO - getAllPosts");
			 e.printStackTrace();
		}
	   	return postsList;
	}

	
	

	
	/**
	 * 	 * @return All posts in notepad for the user as list.
	 */
	
	
	@SuppressWarnings("unchecked")
	public List<Post> getAllNotepadPosts(int viewType , int userID){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		List<Post> postsList = null;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			postsList = (List<Post>)session.createQuery("from Post where viewType="+viewType+" and userID ="+userID).list();
		   	System.out.println("PostsList count = "+postsList.size());	
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Exception error - PostDAO - getAllNotepadPosts");
			 e.printStackTrace();
		}
	   
	   	
	   	return postsList;
	}
	
	/**
	 * Load 10 posts from given number
	 * @param numser of first post
	 * @param number of posts to load
	 * @return Posts by 10 posts starting with given number.
	 */
	@SuppressWarnings("unchecked")
	public List<Post> getAllPostsBy10ForPaginationAjaxRequest(
			int firstPost,
			int numberofPostsToLoad
			){
	   	System.out.println("PL_frogetAllPostsBy10ForPaginationAjaxRequest");

		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();			 
			//count the posts
			//Long postCount = (Long) session.createCriteria(Post.class).setProjection(Projections.rowCount()).uniqueResult();
			
			String strQry = "from Post c where viewType=1 order by c.id desc ";
		        Query query = session.createQuery(strQry);
		        query.setFirstResult(firstPost);
		        query.setMaxResults(numberofPostsToLoad);
		    List<Post> postsList = query.list();   
		   	//System.out.println("PostsList count 1 = "+postCount +"\n q.list"+query.list());
		   	tx.commit();
		   	System.out.println("PL_fromDAO = "+postsList.size());
		   	return postsList;
	   	} catch (Exception e) {
			System.out.println("Exception error with loading ajax posts");
			 e.printStackTrace();
		}
	   	return new ArrayList<>();
	}

	
	
	/**
	 * Get posts count in db by selected viewType		</br > 
	 * 0 - notepads for all								</br>
	 * 1 - public for all 								</br>
	 * used reateCriteria and  Restrictions.eq("viewType", viewType ) </br >
	 * @return postCount (long casted to int)
	 */
	@SuppressWarnings("unchecked")
	public int getPostsNumberInDB(int viewType){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		List<Post> postsList = null;
		int pc = 0;
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			//postCount 
			int postCount = 
					utils.safeLongToInt(
					(long) session.createCriteria(Post.class)
					.add( Restrictions.eq("viewType", viewType ) )
					.setProjection(Projections.rowCount()).uniqueResult()
					);
			return postCount;
	   	} catch (Exception e) {
			System.out.println("Exception error - PostDAO - getPostsNumberInDB");
			 e.printStackTrace();
		}
	   	return 0;
	}
	/**
	 * Return Post by ID.
	 * 
	 * @param postId - Posts ID which we wish to load.
	 * @return	Specified Post
	 */
	
	
	public Post getPostByID(int postId){
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		Post post = Post.getNewPost();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			post = (Post) session.get(Post.class, postId);
			tx.commit();
	   	} catch (Exception e) {
			System.out.println("Exception error - getPostByID");
			 e.printStackTrace();
		}
	   
		System.out.println("Got post, id = "+postId);
	   	return post;
	}

/**	Update a Post object in db.						<br />
 * added throws RuntimeException for testing 		<br />
 * @param post Post which we wish to update. Needs to have a specified ID.<br />	
 */

	public void updatePost(Post post) throws RuntimeException{
		HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
	   	try {
			Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(post);
			tx.commit();
			//Logging				
			//List<String> ut = new Utils().getLoggedUsername();
			//logger.info("User --"+ut.get(0)+"-- updated POST ID = "+post.getId());//after this data dispolays in lower line , why ?
			
	   	} catch (Exception e) {
			System.out.println("Exception - getPostByID");
			 e.printStackTrace();
		}
		System.out.println("Update post = "+post.toString());
	}


	/**	Delete in db a Post object by given ID.
	 * 
	 * @param Needs to have a specified ID.
	 */

		public void deletePost(int id){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		   	try {
				Session session = fabrykaHibernejta.getNewSessionAnnotations(hibfile);
				PostDAO post = new PostDAO();
				Transaction tx = session.beginTransaction();
		   		System.out.println("Before delete");
		   		session.delete(post.getPostByID(id));
		   		System.out.println("After delete");
				tx.commit();
				//Logging				
				//List<String> ut = new Utils().getLoggedUsername();
				//logger.info("User --"+ut.get(0)+"-- deleted POST ID = "+id);//after this data dispolays in lower line , why ?
				
		   	} catch (IllegalArgumentException e) {  
		   		System.out.println(" No post by that id");
				 e.printStackTrace();
	        }  	catch (Exception e) {
				System.out.println("Exception - DeletePostByID");
			}
		}


	
		/**
		 * Method used in DBConfig for saving the Post object with provided opened session (taken from sessionFactory)
		 * @param post
		 * @param sessionProvided
		 * @return "OK" or "NotOK"
		 */
		
		public int savePost(Post post, Session sessionProvided){
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
		   	try {
		   		Session session = sessionProvided;
				Transaction tx = session.beginTransaction();
				post.toString();
				post.saveCreationEditDate();
				session.save(post);
				tx.commit();
				//logging
				//List<String> ut = new Utils().getLoggedUsername();
				//logger.info("User --"+ut.get(0)+"-- saved POST ID = "+post.getId());//after this data dispolays in lower line , why ?
				
				System.out.println("Post id = "+post.getId()+" edited !");
				return post.getId();
			} catch (Exception e) {
				System.out.println("Error saving single posts - blog.config.DBConfig error. ");
				 e.printStackTrace();
					return 0;
			}

		}

//setters getters

		public Logger getLogger() {
			return logger;
		}



		public void setLogger(Logger logger) {
			this.logger = logger;
		}



		public Utils getUtils() {
			return utils;
		}



		public void setUtils(Utils utils) {
			this.utils = utils;
		}



		public String getHibfile() {
			return hibfile;
		}



		public void setHibfile(String hibfile) {
			this.hibfile = hibfile;
		}
	
		
		
		
	

}
