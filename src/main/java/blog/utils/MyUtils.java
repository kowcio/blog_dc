package blog.utils;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;


/**
 *  Class with various functionalities - to keep it simple, better , faster etc...
 *  
 * @author TalentLab1
 *
 */
@Deprecated
public class MyUtils {

	
	static String redir = "<meta http-equiv=\"Refresh\" content=\"4; url=http://localhost:8080/Blog/adminPanel\"/>"; ;
	
	
	
	/**
	 * 	returns a redirect refresh html message that can be added to mav to refresh the page after prefered action
	 * default timeout for the field redir is 4.
	 * @param redirTime
	 * @return	 meta http-equiv=\"Refresh\" content=\""+redirTime+"; url=http://localhost:8080/Blog/adminPanel\"/;
	 */
	public static String getRedirect(int redirTime){
		
		redir = "<meta http-equiv=\"Refresh\" content=\""+redirTime+"; url=http://localhost:8080/Blog/adminPanel\"/>";
		
		return redir;
	}
	
	
	/**
	 * 	returns a redirect refresh html message that can be added to mav to refresh the page after prefered action
	 * 
	 * @param redirTime	timeout to refresh - in seconds
	 * @param url  - url to refresh to 
	 * @return	 meta http-equiv=\"Refresh\" content=\""+redirTime+"; url="+url+"\"/
	 */
	public static String getRedirectMainSite(int redirTime, String url){
		
		redir = "<meta http-equiv=\"Refresh\" content=\""+redirTime+"; url="+url+"\"/>";
		
		return redir;
	}
	
	
	
	/**
	   * Check if a role is present in the authorities of current user
	   * @param authorities all authorities assigned to current user
	   * @param role required authority
	   * @return true if role is present in list of authorities assigned to current user, false otherwise
	   */
	  @SuppressWarnings("unused")
	private boolean isRolePresent(Collection<GrantedAuthority> authorities, String role) {
	    boolean isRolePresent = false;
	    for (GrantedAuthority grantedAuthority : authorities) {
	      isRolePresent = grantedAuthority.getAuthority().equals(role);
	      if (isRolePresent) break;
	    }
	    return isRolePresent;
	  }
	
	
	
	
	
	
}
