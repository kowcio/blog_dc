<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">


<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>

<head>
   <title>Show Posts Blog Index Page</title>	

 	  <!--  /HiSpring3/resources/css/maincss.css - whole path to css files 	-->
 	   <link href=" <c:url value="/resources/css/maincss.css" />" rel="stylesheet" type="text/css" />
 	   
</head>
<body>



<div id="showPosts" class="showPosts">	
<a id="glowna" href="<c:url value="/"/>">	<spring:message code="menu.mainpage.link" text="main page" /> 	</a><br /> 



<c:if test="${postEMPTY=='postListIsNull'}">
${postEMPTY}
</c:if>
	
<div class="displayPosts">
POSTS<br />
	<c:forEach var="post" items="${PostsList}">
	
	<div class="singlePostDiv">
			<a href="${path}/posts/show/${post.id}/"  name="showLink" id="showLink${post.id}">
				<table class="PostTable"><tr>
			 	<td>  ${post.id} </td><td> 	${post.title}		</td></tr>
			 	<tr><td colspan="2">  		${post.content}		</td></tr>
		 		<tr><td colspan="2">  		${post.addDate} 	</td></tr>	
		 	 	</table>
		 	</a><br />		 	 	
		 	 	<!--  security links displayed for admin or user etc. -->		 	 	
		 	 			<sec:authorize access="hasRole('666')">
		 	 					<a href="${path}/adminPanel/post/${post.id}/" name="editLink" id="editLink${post.id}">  
		 	 					<spring:message code="posts.edit" text="ERROR no code" />  </a>
		 	 					
		 	 					<br />
		 	 					
		 	 					<a href="${path}/posts/delete/${post.id}/" onclick="return checkMe()" name="deleteLink"
		 	 					id="deleteLink${post.id}">
		 	 					<spring:message code="posts.delete" text="ERROR no code" />   </a>
						</sec:authorize>
						
						
	    	<!--  ADD COMMENT LINK -->
	    	<br />
	    	
			<a href="${path}/comment/add/${post.id}/" name="addCommentLink" id="addCommentLink${post.id}">
 					<spring:message code="post.addcomments" text="Comment" />   </a>
				
				
				
	    	
	    	<div class="commentsDiv">
	    	
	    		<c:forEach var="comment" items="${post.commentsList}">
	
				<table class="commentsTable">
			 	<tr><td>ID: 	</td><td>        ${comment.id} 	</td></tr>
			 	<tr><td>Content:</td><td>   	 ${comment.content}</td></tr>
		 		<tr><td>  Nick :</td><td>		 ${comment.nick}	</td></tr>  	 
		 		<tr><td>  Mail :</td><td> 		 ${comment.mail}	</td></tr>	
		 		<tr><td>  VT :  </td><td> 		 ${comment.viewType}</td></tr>
		 	 	</table>
			
			</c:forEach>
	    	</div>
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	</div>
	</c:forEach>
	
	    
</div> <!--  the posts div -->	
</div>


	
</body>
</html>