<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html;charset=UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">


<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>

<head>
   <title>Single Posts </title>	

 	  <!--  /HiSpring3/resources/css/maincss.css - whole path to css files 	-->
 	   <link href=" <c:url value="/resources/css/maincss.css" />" rel="stylesheet" type="text/css" />
 	   
</head>
<body>




<c:if test="${postEMPTY=='postListIsNull'}">
${postEMPTY}
</c:if>

		<table class="PostTable"><tr>
	 	<td>  ${post.id} </td><td> 	${post.title}		</td></tr>
	 	<tr><td colspan="2">  		${post.content}		</td></tr>
 		<tr><td colspan="2">  		${post.addDate} 	</td></tr>
 	 	</table>
	 
	
	

	
</body>
</html>