<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    
<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>
	




<div id="showPosts" class="showPosts">	

     
<!--  PAGINATION -->  
    <div class="pagination text-center">
    <ul>
    <li><a  class="pageBar" href="#" onclick="loadFirstAjaxPosts()" id="firstPage"> First   </a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxPosts(0)"   id="page1">1</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxPosts(10)"    id="page2">2</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxPosts(20)"    id="page3">3</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxPosts(30)"    id="page4">4</a></li>
    <li><a  class="pageBarNo" href="#" onclick="loadAjaxPosts(40)"    id="page5">5</a></li>
    <li><a  class="pageBar" href="#" onclick="loadLastAjaxPosts(${lastPosts})"  id="lastPage">  
    <spring:message code="menu.last" text="Last" /> 10
	
    <spring:message code="menu.from" text="from"/> 
    <span id="postsCount" style="visibility:none">
    ${lastPosts}
    </span>
    </a>
	</li>
    </ul>
    </div>
    
<c:if test="${postEMPTY=='postListIsNull'}">${postEMPTY}</c:if> 
	
<script type="text/javascript">
//script for changing div with the content returned from urls
function loadAjaxPosts(postNo)
{        $(".displayPosts").load("${path}/posts/nextpart/"+postNo);  
	 
		 //changing the onclick values 
		 	 var i = Math.floor(postNo/10) -2;
			 var j = 20;
			 var basicValues=0;
		 $( ".pageBarNo" ).each(function( index ) {
				i++;
			 //for first, second and third webpage
				if (postNo <= 20){
					basicValues+=10;
					$(this).attr("onclick","loadAjaxPosts("+ (basicValues-10)	 +")");
					$(this).text(basicValues/10);
				}
								
				//if (postNo > 20){
				else{
					$(this).attr("onclick","loadAjaxPosts("+ ( postNo - j)	 +")");
				 j-=10;
			 	 var onClick =  $(this).attr("onclick");				
				 //alert( 	onClick		+" check div = "	);
				 
				$(this).text(i);
			 }
			// console.log( index + ": " + $(this).text() 	);
		 });
}   
function loadLastAjaxPosts(postNo)
{        $(".displayPosts").load("${path}/posts/nextpart/${lastPosts}");
			postNo = Math.floor(postNo / 10) * 10 ;
			//alert(postNo);
			loadAjaxPosts(postNo);              
}   
function loadFirstAjaxPosts()
{		 
			loadAjaxPosts(0);              
}       
</script>

<div class="displayPosts">
<c:forEach var="post" items="${ajaxPostsList}">
	
	<div class="singlePostDiv">
			<a href="${path}/posts/show/${post.id}/"  name="showLink" id="showLink${post.id}">				
				<table class="PostTable"><tr>
			 	<td>  ${post.id} </td><td> 	${post.title}		</td></tr>
			 	<tr><td colspan="2">  		${post.content}		</td></tr>
		 		<tr><td colspan="2">  		${post.addDate} 	</td></tr>	
		 	 	</table>		 	 	
		 	</a><br />		 	 	
		 	 	<!--  security links displayed for admin or user etc. -->		 	 	
		 	 	<sec:authorize access="hasRole('666')">
	       	 	 	<a href="${path}/adminPanel/post/${post.id}/" name="editLink" id="editLink${post.id}">  
		 	 		<spring:message code="posts.edit" text="ERROR no code" />  </a>
		 	 		
		 	 					<br />
		 	 					
		 	 		<a href="${path}/posts/delete/${post.id}/" onclick="return checkMe()" name="deleteLink" id="deleteLink${post.id}">
 					<spring:message code="posts.delete" text="ERROR no code" />   </a>
				</sec:authorize>


	    		<!--  ADD COMMENT LINK -->
	    	<br />
	    	
			<a href="${path}/comment/add/${post.id}/" name="addCommentLink" id="addCommentLink${post.id}">
 					<spring:message code="post.addcomments" text="Comment" />   </a>
				
	    	
	    	
<!--  COMMENTS DIV  -->	    	
    	<div class="commentsDiv">
	    	
	    		<c:forEach var="comment" items="${post.commentsList}">
	
				<table class="commentsTable">
			 	<tr><td>ID: 	</td><td>        ${comment.id} 	</td></tr>
			 	<tr><td>Content:</td><td>   	 ${comment.content}</td></tr>
		 		<tr><td>  Nick :</td><td>		 ${comment.nick}	</td></tr>  	 
		 		<tr><td>  Mail :</td><td> 		 ${comment.mail}	</td></tr>	
		 		<tr><td>  VT :  </td><td> 		 ${comment.viewType}</td></tr>
		 	 	</table>
			
			</c:forEach>
	    	</div>
	





	</div>
	</c:forEach>




</div> <!--  the posts div -->	




</div>
