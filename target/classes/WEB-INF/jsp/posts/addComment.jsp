<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html;charset=UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">


<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>

<head>
   <title>Add comment  </title>	

 	  <!--  /HiSpring3/resources/css/maincss.css - whole path to css files 	-->
 	   <link href=" <c:url value="/resources/css/maincss.css" />" rel="stylesheet" type="text/css" />
 	   
</head>
<body>


		<table class="PostTable"><tr>
	 	<td>  ${post.id} </td><td> 	${post.title}		</td></tr>
	 	<tr><td colspan="2">  		${post.content}		</td></tr>
 		<tr><td colspan="2">  		${post.addDate} 	</td></tr>
 	 	</table>
	 
	 
	 <!--  form for adding the comment  -->
	 
	 <form:form method="POST" action="${path}/comment/add/${post.id}" modelAttribute="comment" id="addCommentForm">
		<table border=1><tr><td>
					<form:label path="nick"><spring:message code="comment.add.nick" text="title" /></form:label>
					<form:input path="nick"/>
				
				<br/>
					
					<form:label path="mail"><spring:message code="comment.add.mail" text="mail" /></form:label>
					<form:input path="mail"/>
				
				</td></tr><tr><td>
				
					<form:label path="content"><spring:message code="comment.add.content" text="content" /></form:label>
					<form:textarea path="content"   rows="5" cols="30" />
					
				</td></tr><tr><td>
				<!--  hidden -->
				<form:hidden path="postID"/>
				<!-- errors -->
				<form:errors path="nick" 	 cssClass="error"/>
				<form:errors path="mail" 	 cssClass="error"/>
				<form:errors path="content"  cssClass="error"/>
				<input type="submit" value="<spring:message code="post.add.addbtn" text="Add" />" id="submit"/>
		</td></tr></table>
		</form:form>
	
	

	
</body>
</html>